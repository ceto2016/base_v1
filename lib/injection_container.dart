import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../core/util/navigator.dart';
import 'core/services/services/notifications/local_notifications.dart';
import 'core/util/api_basehelper.dart';
import 'core/util/app_location.dart';
import 'features/auth/auth_injection.dart';
import 'features/books/books_injection.dart';
import 'features/more/more_inj.dart';

final GetIt sl = GetIt.instance;
final ApiBaseHelper helper = ApiBaseHelper();
Future<void> init() async {
  initAuthInjection();
  initMoreInjection();
  initBooksInjection();
  _initNotifications();
  // core
  sl.registerLazySingleton<AppNavigator>(() => AppNavigator());
  final SharedPreferences sharedPreferences =
      await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  helper.dioInit();
  sl.registerLazySingleton(() => helper);
}

void injectFCMToken(String? fcmToken) {
  if (sl.isRegistered(instance: fcmToken, instanceName: 'fcmToken')) {
    sl.unregister(instanceName: 'fcmToken');
  }
  sl.registerLazySingleton<String>(() => fcmToken ?? '',
      instanceName: 'fcmToken');
}

String get fcmToken => sl<String>(instanceName: 'fcmToken');

void _initNotifications() {
  sl.registerLazySingleton<LocalNotificationService>(
      () => LocalNotificationService());
}

AppNavigator get appNavigator => sl<AppNavigator>();

AppLocation get appLocation => sl<AppLocation>();
