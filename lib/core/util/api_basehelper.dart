import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../features/auth/presentation/cubit/auto_login/auto_login_cubit.dart';
import '../../injection_container.dart';
import '../error/exceptions.dart';
import '../local/auth_local_datasource.dart';
import '../widgets/toast.dart';

const String googleServicesAPIKey = '';

class ApiBaseHelper {
  final String _baseUrl = 'https://backend.AwamerBase.net/client-api/v1';
  final Dio dio = Dio();
  void dioInit() {
    dio.options.baseUrl = _baseUrl;
    dio.options.headers = headers;
    dio.options.sendTimeout = const Duration(milliseconds: 30000); // time in ms
    dio.options.connectTimeout =
        const Duration(milliseconds: 30000); // time in ms
    dio.options.receiveTimeout = const Duration(milliseconds: 30000);
  }

  void updateLocalInHeaders(String local) {
    if (local == 'ur') {
      local = 'ad';
    }
    headers['Lang'] = local;
    headers['Accept-language'] = local;
    dio.options.headers = headers;
  }

  void addTokenInHeader(String token) {
    headers['Authorization'] = 'Bearer $token';
    dio.options.headers = headers;
  }

  void removeTokenInHeader() {
    headers.remove('Authorization');
    dio.options.headers = headers;
  }

  Map<String, String> headers = <String, String>{
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  };

  logRequestInfo(String url, {dynamic response, String? body}) {
    log('url: $url');
    log('body: $body');
    log('response: $response');
    log('headers: $headers');
  }

  throwInternetExcpetion(DioException e) {
    log('e: $e');
    log('error response: ${e.response}');
    if (e.type == DioExceptionType.sendTimeout ||
        e.type == DioExceptionType.receiveTimeout ||
        e.type == DioExceptionType.connectionTimeout) {
      String message = tr('error_no_internet');
      throw ServerException(message: message);
    }
  }

  Future<dynamic> get({required String url}) async {
    try {
      logRequestInfo(
        url,
      );
      final Response response = await dio.get(url);
      final responseJson = _returnResponse(response);
      logRequestInfo(
        url,
      );
      return responseJson;
    } on SocketException {
      throw ServerException(message: tr('error_no_internet'));
    } on DioException catch (e) {
      throwInternetExcpetion(e);
      logRequestInfo(url);
      _returnResponse(e.response);
    }
  }

  Future<dynamic> put({
    required String url,
    Map<String, dynamic>? body,
  }) async {
    try {
      Response? response;
      if (body != null) {
        response = await dio.put(url, data: body);
      } else {
        response = await dio.put(url);
      }
      final responseJson = _returnResponse(response);
      logRequestInfo(
        url,
        response: responseJson,
      );
      return responseJson;
    } on SocketException {
      throw ServerException(message: tr('error_no_internet'));
    } on DioException catch (e) {
      throwInternetExcpetion(e);
      logRequestInfo(url);
      _returnResponse(e.response);
    }
  }

  Future<dynamic> post({
    required String url,
    required Map<String, dynamic> body,
  }) async {
    try {
      FormData formData = FormData.fromMap(body);
      final Response response = await dio.post(url, data: formData);
      final responseJson = _returnResponse(response);
      logRequestInfo(
        url,
        response: responseJson,
        body: body.toString(),
      );
      return responseJson;
    } on SocketException {
      throw ServerException(message: tr('error_no_internet'));
    } on DioException catch (e) {
      throwInternetExcpetion(e);
      logRequestInfo(
        url,
        body: body.toString(),
      );
      _returnResponse(e.response);
    }
  }

  Future<dynamic> postWithRaw({
    required String url,
    required Map<String, dynamic> body,
  }) async {
    try {
      final Response response = await dio.post(url, data: body);
      final responseJson = _returnResponse(response);
      logRequestInfo(url, response: responseJson);
      return responseJson;
    } on SocketException {
      throw ServerException(message: tr('error_no_internet'));
    } on DioException catch (e) {
      throwInternetExcpetion(e);
      logRequestInfo(url);
      _returnResponse(e.response);
    }
  }

  Future<dynamic> postWithImage({
    required String url,
    required Map<String, dynamic> body,
  }) async {
    try {
      dio.options.sendTimeout =
          const Duration(milliseconds: 30000); // time in ms
      dio.options.connectTimeout =
          const Duration(milliseconds: 30000); // time in ms
      dio.options.receiveTimeout = const Duration(milliseconds: 30000);

      FormData formData = FormData.fromMap(body);
      final Response response = await dio.post(url, data: formData);
      final responseJson = _returnResponse(response);
      logRequestInfo(url, response: responseJson);
      dio.options.sendTimeout =
          const Duration(milliseconds: 8000); // time in ms
      dio.options.connectTimeout =
          const Duration(milliseconds: 8000); // time in ms
      dio.options.receiveTimeout = const Duration(milliseconds: 8000);
      return responseJson;
    } on SocketException {
      throw ServerException(message: tr('error_no_internet'));
    } on DioException catch (e) {
      throwInternetExcpetion(e);
      logRequestInfo(url);
      _returnResponse(e.response);
    }
  }

  Future<dynamic> delete({
    required String url,
  }) async {
    try {
      // headers["Content-language"] = local;

      final Response response = await dio.delete(url);

      final responseJson = _returnResponse(response);
      logRequestInfo(url, response: responseJson);
      return responseJson;
    } on SocketException {
      throw ServerException(message: tr('error_no_internet'));
    } on DioException catch (e) {
      throwInternetExcpetion(e);
      logRequestInfo(url);
      _returnResponse(e.response);
    }
  }

  Future<dynamic> uploadImage({required String url, required File file}) async {
    try {
      String fileName = file.path.split('/').last;
      FormData formData = FormData.fromMap(<String, dynamic>{
        'file': await MultipartFile.fromFile(file.path, filename: fileName),
      });
      final Response response = await dio.post(url, data: formData);
      final responseJson = _returnResponse(response);
      logRequestInfo(url, response: responseJson);
      return responseJson;
    } on SocketException {
      throw ServerException(message: tr('error_no_internet'));
    } on DioException catch (e) {
      throwInternetExcpetion(e);
      logRequestInfo(url);
      _returnResponse(e.response);
    }
  }

  //
  unAuth401(String message) {
    showErrorToast(message);
    appNavigator.popToFrist();
    sl<AuthLocalDataSource>().clearData();
    sl<AutoLoginCubit>().emitNoUser();
  }

  dynamic _returnResponse(Response? response) {
    if (response == null) {
      throw ServerException(
        message: tr('error_no_internet'),
      );
    }
    log('statusCode: ${response.statusCode}');
    switch (response.statusCode) {
      case 200:
      case 201:
        return response.data;
      case 400:
      case 422:
        final data = response.data;
        throw ServerException(message: data['message'].toString());
      case 401:
      case 403:
        final data = response.data;
        unAuth401(data['message'].toString());
        throw ServerException(message: response.data);
      case 500:
      default:
        throw ServerException(message: tr('error_no_internet'));
    }
  }
}
