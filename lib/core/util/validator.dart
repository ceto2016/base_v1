import 'package:easy_localization/easy_localization.dart';

import '../widgets/inti_phone_form_feild.dart';
class Validator {
  static String? ddlValidator<T>(T? value) {
    if (value == null) {
      return tr('error_filed_required');
    }

    return null;
  }

  static String? defaultValidator(String? value) {
    if (value == null || value.trim().isEmpty) {
      return tr('error_filed_required');
    }

    return null;
  }

  static String? name(String? value) {
    if (value != null) {
      value = value.trim();
      if (value.isEmpty) {
        return tr('enter_correct_name');
      }
      if (value.length < 3 || value.length > 20) {
        return tr('name_short_input');
      }
    }
    return null;
  }

  static String? fastOrder(String? value) {
    if (value != null) {
      value = value.trim();
      if (value.isEmpty) {
        return tr('error_filed_required');
      }
      if (value.length < 3) {
        return tr('name_short_input');
      }
    }
    return null;
  }

  static String? registerAddress(String? value) {
    if (value != null) {
      value = value.trim();
      if (value.isEmpty) {
        return tr('error_filed_required');
      }
      if (value.length < 4) {
        return tr('short_address');
      }
    }
    return null;
  }

  static String? text(String? value) {
    if (value != null) {
      value = value.trim();
      if (value.isEmpty) {
        return tr('error_filed_required');
      } else if (!RegExp('[a-zA-Z]').hasMatch(value)) {
        return tr('enter_correct_name');
      }
    }
    return null;
  }

  static String? defaultEmptyValidator(String? value) {
    return null;
  }

  static String? email(String? value) {
    if (value != null) {
      value = value.trim();
      if (value.isEmpty) {
        return tr('error_filed_required');
      } else if (!RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(value)) {
        return tr('emailValidation');
      }
    } else {
      return tr('error_filed_required');
    }
    return null;
  }

  static String? password(String? value) {
    // RegExp strongPassword =
    //     RegExp(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!%*?&]{8,}$');

    if (value != null) {
      value = value.trim();
      if (value.isEmpty) {
        return tr('error_filed_required');
      }

      // if (!strongPassword.hasMatch(value)) {
      //   return tr('error_password_validation');
      // }
    }
    return null;
  }

  static String? confirmPassword(String? confirmPassword, String? password) {
    if (confirmPassword != null) {
      confirmPassword = confirmPassword.trim();
      if (confirmPassword.isEmpty) {
        return tr('error_filed_required');
      } else if (confirmPassword != password) {
        return tr('password_mismatch');
      }
    } else {
      return tr('error_filed_required');
    }
    return null;
  }

  static String? numbers(String? value) {
    if (value != null) {
      value = value.trim();
      if (value.isEmpty) {
        return tr('error_filed_required');
      }
      if (value.startsWith('+')) {
        value = value.replaceFirst(r'+', '');
      }
      final int? number = int.tryParse(value);
      if (number == null) {
        return tr('error_wrong_input');
      }
    } else {
      return tr('error_filed_required');
    }
    return null;
  }

  static String? phone(String? value, PhoneModel model) {
    if (value != null) {
      value = value.trim();
      value = value.replaceAll(r' ', '');
      if (value.isEmpty) {
        return 'ادخل رقم الهاتف';
      }
      if (!value.startsWith(model.startWith) || value.length != model.lenght) {
        return "${"يجب ان يبدأ ب ${model.startWith}"} و يتكون من ${model.lenght} أرقام ";
      }
    }
    return null;
  }
}
