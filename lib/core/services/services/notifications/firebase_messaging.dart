import 'dart:developer';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';

import '../../../../injection_container.dart';
import 'local_notifications.dart';

abstract class AppFirebaseMessaging {
  static Future<void> setForegroundNotificationPresentationOptions() async {
    return await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );
  }

  static Future<void> getToken() async {
    try {
      final String? fcmToken = await FirebaseMessaging.instance.getToken();
      injectFCMToken(fcmToken);
      log('FCM token: ${fcmToken.toString()}');
    } catch (e) {
      log(e.toString());
    }
  }

  static void onTokenRefresh() {
    FirebaseMessaging.instance.onTokenRefresh.listen((fcmToken) {
      injectFCMToken(fcmToken);
      log('FCM onTokenRefresh: ${fcmToken.toString()}');
    });
  }

  static void onBackgroundMessage(LocalNotificationService service) {
    FirebaseMessaging.onBackgroundMessage((event) async {
      _showLocalNotification(service, event);
    });
  }

  static void onMessage(LocalNotificationService service) {
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      log('FCM onMessage: ${event.notification?.title}');
      log('FCM onMessage: ${event.notification?.body}');

      _showLocalNotification(service, event);
    });
  }

  static void onMessageOpenedApp(LocalNotificationService service) {
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage event) {
      log('FCM onMessageOpenedApp: ${event.notification?.title}');
      // _showLocalNotification(service, event);
    });
  }

  static void subscribeToTopic(String topic) {
    FirebaseMessaging.instance.subscribeToTopic(topic).then((value) {
      log('FCM subscribeToTopic: Success');
    }).catchError((error) {
      log('FCM subscribeToTopic: Error: ${error.toString()}');
    });
  }

  static void _showLocalNotification(
      LocalNotificationService service, RemoteMessage event) {
    String title = event.notification?.title ?? '';
    String body = event.notification?.body ?? '';
    if (Platform.isAndroid) {
      service.showNotification(id: 100, title: title, body: body);
    }
  }
}
