// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';
import 'dart:developer';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';

import '../../../../features/auth/presentation/cubit/auto_login/auto_login_cubit.dart';
import '../../../../injection_container.dart';
import 'dynamic_models.dart';

StreamSubscription? subscription;
Future<void> initDynamicLinks() async {
  subscription?.cancel();
  subscription =
      FirebaseDynamicLinks.instance.onLink.listen((dynamicLinkData) async {
    final Uri deepLink = dynamicLinkData.link;

    await handlingDynamicLinks(deepLink: deepLink);
    debugPrint('DEEP LINK 111: ${deepLink.path}', wrapWidth: 1024);
    debugPrint('DEEP LINK 222: $deepLink}', wrapWidth: 1024);
  })
        ..onError((error) {
          debugPrint(
              '============= initDynamicLinks ERROR: ${error.toString()} =============');
        });
  final PendingDynamicLinkData? initialLink =
      await FirebaseDynamicLinks.instance.getInitialLink();
  log('============= initDynamicLinks success =============');
  if (initialLink != null) {
    final Uri deepLink = initialLink.link;
    debugPrint('DEEP LINK: ${deepLink.path}', wrapWidth: 1024);
    debugPrint('DEEP LINK: $deepLink}', wrapWidth: 1024);
    handlingDynamicLinks(deepLink: deepLink);
  }
}

Timer? time;
Future<void> handlingDynamicLinks({required Uri deepLink, int? id}) async {
  time?.cancel();
  time = Timer(const Duration(milliseconds: 500), () {
    log(':rocket: ~ file: dynamic_links_service.dart ~ line 34 ~ void handlingDynamicLinks ~ $deepLink');
    if (deepLink.queryParameters['surahId'] != null) {
      final ShareContentAbs shareContent = deepLink.shareFromUrl();

      if (sl<AutoLoginCubit>().isUser) {
        shareContent.navigate();
      }
    }
  });
}

Future<void> createDynamicLink(
  ShareContentAbs dataParams, {
  bool short = true,
  required String surahTitle,
  required int page,
  String? ayaTitle,
}) async {
  try {
    String link = 'https://AwamerBase.net${dataParams.toParams()}';
    log(link);
    FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;

    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://AwamerBase.page.link',
      link: Uri.parse(link),
      androidParameters: const AndroidParameters(
        packageName: 'com.awamer.base',
        minimumVersion: 1,
      ),
      iosParameters: const IOSParameters(
        bundleId: 'com.awamer.base',
        appStoreId: '6476266349',
        minimumVersion: '0',
      ),
    );

    final Uri uri;
    if (short) {
      final ShortDynamicLink shortDynamicLink =
          await FirebaseDynamicLinks.instance.buildShortLink(parameters);
      uri = shortDynamicLink.shortUrl;
    } else {
      uri = await dynamicLinks.buildLink(parameters);
    }
    log(uri.toString());
    String shareContent =
        "AwamerBase \n $surahTitle ( $page ) ${"\n ${ayaTitle ?? ""}"} \n ${uri.toString()}";
    Share.share(shareContent);
  } catch (e) {
    log(e.toString());
  }
}

Future<void> shareApp({
  bool short = true,
}) async {
  try {
    String link = 'https://AwamerBase.net';
    log(link);
    FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;

    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://AwamerBase.page.link',
      link: Uri.parse(link),
      androidParameters: const AndroidParameters(
        packageName: 'com.awamer.base',
        minimumVersion: 1,
      ),
      iosParameters: const IOSParameters(
        bundleId: 'com.awamer.base',
        appStoreId: '6476266349',
        minimumVersion: '0',
      ),
    );

    final Uri uri;
    if (short) {
      final ShortDynamicLink shortDynamicLink =
          await FirebaseDynamicLinks.instance.buildShortLink(parameters);
      uri = shortDynamicLink.shortUrl;
    } else {
      uri = await dynamicLinks.buildLink(parameters);
    }
    log(uri.toString());
    String shareContent = 'AwamerBase \n ${uri.toString()}';
    Share.share(shareContent);
  } catch (e) {
    log(e.toString());
  }
}
