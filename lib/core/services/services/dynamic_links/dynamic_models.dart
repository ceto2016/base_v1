abstract class ShareContentAbs {
  ShareContentAbs();
  String toParams();
  void navigate();
}

class ShareContent implements ShareContentAbs {
  @override
  void navigate() {
    // TODO: implement navigate
  }

  @override
  String toParams() {
    // TODO: implement toParams
    throw UnimplementedError();
  }
}

extension ShareContentExt on Uri {
  ShareContentAbs shareFromUrl() {
    return ShareContent();
  }
}
