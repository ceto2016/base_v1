// import 'dart:developer';

// import 'package:awesome_notifications/awesome_notifications.dart';

// import '../../../injection_container.dart';
// import '../../../main.dart';
// import '../../constant/values/colors.dart';

// const String _log = '@AwesomeNotification:';
// class AwesomeNotificationsService {
//   final AwesomeNotifications _awesomeNotifications;

//   AwesomeNotificationsService({
//     required AwesomeNotifications awesomeNotifications,
//   }) : _awesomeNotifications = awesomeNotifications;

//   final String _channelKey = 'basic_channel';
//   final String _channelName = 'basic_channel';
//   final String _channelGroupKey = 'basic_channel_group';
//   final String _channelGroupName = 'Basic group';

//   Future<void> init() async{
//     final bool isInitialized = await _initialize();
//     log('$_log isInitialized $isInitialized');
//     if(isInitialized){
//       final bool isNotificationAllowed =  await AwesomeNotifications().isNotificationAllowed();
//       log('$_log isNotificationAllowed $isNotificationAllowed');
//       if(!isNotificationAllowed){
//         final bool isRequestPermission = await AwesomeNotifications().requestPermissionToSendNotifications();
//         log('$_log isRequestPermission $isRequestPermission');
//       }
//       // final bool isListeners = await _setListeners();
//       // log('$_log isListeners $isListeners');
//     }
//   }

//   Future<bool> _initialize(){
//     return AwesomeNotifications().initialize(
//       // set the icon to null if you want to use the default app icon
//       null,
//       <NotificationChannel>[
//         NotificationChannel(
//           channelGroupKey: _channelGroupKey,
//           channelKey: _channelKey,
//           channelName: _channelName,
//           channelDescription: 'Notification channel for basic tests',
//           defaultColor: AppColors.primary,
//           ledColor: AppColors.white,
//           importance: NotificationImportance.Max,
//           channelShowBadge: true,
//           onlyAlertOnce: true,
//           playSound: true,
//           criticalAlerts: true,
//         ),
//       ],
//       // Channel groups are only visual and are not required
//       channelGroups: <NotificationChannelGroup>[
//         NotificationChannelGroup(
//           channelGroupKey: _channelGroupKey,
//           channelGroupName: _channelGroupName,
//         )
//       ],
//       debug: true,
//     );
//   }

//   Future<bool> setListeners(){
//     // Only after at least the action method is set, the notification events are delivered
//     return AwesomeNotifications().setListeners(
//       onActionReceivedMethod: AwesomeNotificationController.onActionReceivedMethod,
//       onNotificationCreatedMethod: AwesomeNotificationController.onNotificationCreatedMethod,
//       onNotificationDisplayedMethod: AwesomeNotificationController.onNotificationDisplayedMethod,
//       onDismissActionReceivedMethod: AwesomeNotificationController.onDismissActionReceivedMethod,
//     );
//   }

//   Future<void> createNotification({
//     required final int id,
//     required final String title,
//     required final String body,
//     final String? summary,
//     final Map<String, String>? payload,
//     final ActionType actionType = ActionType.Default,
//     final NotificationLayout notificationLayout = NotificationLayout.Default,
//     final NotificationCategory? category,
//     final String? bigPicture,
//     final List<NotificationActionButton>? actionButtons,
//     final bool isScheduled = false,
//     final int? interval,
//   }) async{
//     assert(!isScheduled || (isScheduled && interval != null));
//     await AwesomeNotifications().createNotification(
//       content: NotificationContent(
//         id: id,
//         channelKey: _channelKey,
//         title: title,
//         body: body,
//         actionType: actionType,
//         notificationLayout: notificationLayout,
//         summary: summary,
//         category: category,
//         payload: payload,
//         bigPicture: bigPicture,
//       ),
//       actionButtons: actionButtons,
//       schedule: isScheduled
//           ? await getNotificationInterval(interval)
//           : null,
//     );
//   }

//   Future<NotificationInterval> getNotificationInterval(int? interval) async{
//     return NotificationInterval(
//       interval: interval,
//       timeZone: await AwesomeNotifications().getLocalTimeZoneIdentifier(),
//       preciseAlarm: true,
//     );
//   }
// }

// class AwesomeNotificationController {
//   /// Use this method to detect when a new notification or a schedule is created
//   @pragma('vm:entry-point')
//   static Future<void> onNotificationCreatedMethod(
//       ReceivedNotification receivedNotification) async {
//     log('$_log onNotificationCreatedMethod');
//     // Your code goes here
//   }

//   /// Use this method to detect every time that a new notification is displayed
//   @pragma('vm:entry-point')
//   static Future<void> onNotificationDisplayedMethod(
//       ReceivedNotification receivedNotification) async {
//     log('$_log receivedNotification');
//     // Your code goes here
//   }

//   /// Use this method to detect if the user dismissed a notification
//   @pragma('vm:entry-point')
//   static Future<void> onDismissActionReceivedMethod(
//       ReceivedAction receivedAction) async {
//     log('$_log onDismissActionReceivedMethod');
//     // Your code goes here
//   }

//   /// Use this method to detect when the user taps on a notification or action button
//   @pragma('vm:entry-point')
//   static Future<void> onActionReceivedMethod(ReceivedAction receivedAction) async {
//     log('$_log onActionReceivedMethod');
//     if(receivedAction.payload != null
//         && receivedAction.payload!['navigate'] != null
//         && receivedAction.payload!['navigate']! == 'true'){
//       appNavigator.push(screen: const NotificationScreen());
//     }
//     // Your code goes here
//     // Navigate into pages,
//     // avoiding to open the notification details page over another details page already opened
//   }
// }
