import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:store_redirect/store_redirect.dart';

import '../../constant/values/colors.dart';
import '../../constant/values/size_config.dart';
import '../../constant/values/strings.dart';
import '../../constant/values/text_styles.dart';
import '../../widgets/app_logo.dart';
import '../../widgets/app_spacer.dart';
import 'update_type_enum.dart';

class AppUpdateDialog extends StatelessWidget {
  final String newVersion;
  final AppUpdateType updateType;

  const AppUpdateDialog({
    required this.newVersion,
    required this.updateType,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: updateType == AppUpdateType.immediately,
      onPopInvoked: (v) async {
        // if (updateType == AppUpdateType.immediately) {
        //   return false;
        // }
        // return true;
      },
      child: AlertDialog(
        titlePadding: EdgeInsetsDirectional.zero,
        contentPadding: EdgeInsetsDirectional.zero,
        actionsPadding: EdgeInsetsDirectional.zero,
        buttonPadding: EdgeInsetsDirectional.zero,
        iconPadding: EdgeInsetsDirectional.zero,
        insetPadding: EdgeInsets.zero,
        backgroundColor: Colors.transparent,
        content: _AppUpdateDialogContent(newVersion: newVersion),
      ),
    );
  }
}

class _AppUpdateDialogContent extends StatelessWidget {
  final String newVersion;

  const _AppUpdateDialogContent({
    required this.newVersion,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: SizeConfig.screenWidth * 0.5,
      height: SizeConfig.screenHeight * 0.5,
      child: Center(
        child: Stack(
          children: <Widget>[
            ///Content
            PositionedDirectional(
              top: 50.h,
              start: 0,
              end: 0,
              child: Container(
                padding: EdgeInsetsDirectional.only(
                  start: SizeConfig.hPadding * 2,
                  end: SizeConfig.hPadding * 2,
                  top: SizeConfig.vPadding * 4,
                  bottom: SizeConfig.vPadding * 2,
                ),
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius:
                      BorderRadius.all(Radius.circular(SizeConfig.radius * 2)),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ///Header
                    Row(
                      children: <Widget>[
                        Text(
                          AppStrings.updateAppTitle,
                          style: TextStyles.semiBold18(),
                        ),
                        const Expanded(child: SizedBox()),
                        Text(
                          'v$newVersion',
                          style:
                              TextStyles.semiBold18(color: AppColors.primary),
                        ),
                      ],
                    ),
                    8.sh,
                    

                    ///Body
                    Text(
                      AppStrings.updateAppBody,
                      style: TextStyles.medium14(color: AppColors.greyDark),
                      maxLines: 4,
                    ),
                    16.sh,

                    ///UpdateButton
                    Center(
                      child: ElevatedButton(
                        onPressed: () async {
                          final PackageInfo packageInfo =
                              await PackageInfo.fromPlatform();
                          StoreRedirect.redirect(
                            androidAppId: packageInfo.packageName,
                            iOSAppId: packageInfo.packageName,
                          );
                        },
                        child: Text(AppStrings.update),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            ///AppLogo
            PositionedDirectional(
              start: 0,
              end: 0,
              child: Container(
                width: 100.w,
                height: 100.h,
                decoration: BoxDecoration(
                  color: AppColors.white,
                  shape: BoxShape.circle,
                  border: Border.all(color: AppColors.secondary, width: 2.w),
                ),
                child: const Center(
                  child: AppLogo(
                    width: 100,
                    height: 100,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
