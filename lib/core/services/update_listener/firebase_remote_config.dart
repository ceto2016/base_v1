// ignore_for_file: constant_identifier_names

import 'dart:developer';

import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

import 'app_update_listener.dart';
import 'update_type_enum.dart';

const String _VERSION_KEY = 'version';
const String _SHOW_UPDATE_DIALOG_KEY = 'showUpdateDialog';
const String _UPDATE_TYPE_KEY = 'updateType';

class FirebaseRemoteConfigService {
  final FirebaseRemoteConfig _instance = FirebaseRemoteConfig.instance;

  FirebaseRemoteConfigService() {
    _init();
  }

  Future<void> _init() async {
    _instance.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(seconds: 60),
      minimumFetchInterval: const Duration(seconds: 1),
    ));
    await _instance.fetchAndActivate();
  }

  String _getAppLatestVersion() {
    return _instance.getString(_VERSION_KEY);
  }

  bool _getShowUpdateDialog() {
    return _instance.getBool(_SHOW_UPDATE_DIALOG_KEY);
  }

  AppUpdateType _getAppUpdateType() {
    String updateTypeStr = _instance.getString(_UPDATE_TYPE_KEY);
    if (updateTypeStr == AppUpdateType.immediately.name) {
      return AppUpdateType.immediately;
    }
    return AppUpdateType.flexible;
  }

  Future<String> _getAppCurrentVersion() async {
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    final String version = packageInfo.version;
    final String buildNumber = packageInfo.buildNumber;
    log('@RemoteConfigService: PackageInfo version: $version, buildNumber: $buildNumber');
    return '$version+$buildNumber';
  }

  Future<void> checkForUpdates(BuildContext context) async {
    _getAppCurrentVersion().then((String currentVersion) {
      String latestVersion = _getAppLatestVersion();
      AppUpdateType updateType = _getAppUpdateType();
      bool showUpdateDialog = _getShowUpdateDialog();
      log('@RemoteConfigService: '
          'showUpdateDialog: $showUpdateDialog, '
          'currentVersion: $currentVersion, '
          'latestVersion: $latestVersion, '
          'updateType: $updateType');
      if (showUpdateDialog && currentVersion != latestVersion) {
        _showAppUpdateDialog(context, latestVersion, updateType);
      }
    });
  }

  void _showAppUpdateDialog(
      BuildContext context, String version, AppUpdateType updateType) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AppUpdateDialog(
          newVersion: version.split('+').first,
          updateType: updateType,
        );
      },
    );
  }
}
