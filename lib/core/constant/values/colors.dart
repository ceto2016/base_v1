import 'package:flutter/material.dart';

abstract class AppColors {
  static bool get isDark => false; //sl<AppThemeCubit>().state.isDarkMode;
  static Color get primary => const Color(0xFFB97E12);
  static const Color secondary = Color(0xFFECCB34);
  static Color get bgColor =>
      isDark ? const Color(0xFF121212) : scaffoldBackgroundColor;
  static Color get cardColor =>
      isDark ? const Color(0xFF1C1C1E) : const Color(0xFFf7f7f7);
  static const Color borderColor = Color(0xFF4D4D4D);
  static Color get textColor =>
      isDark ? const Color(0xFFF0F0F0) : const Color(0xFF121212);
  static const Color inputFieldTextColor = Color(0xFF959595);
  static Color get buttonColor => isDark ? const Color(0xFF383c41) : primary;
  // static Color get textbuttonColor => isDark ?primary: Colors.white;

  static const Color greyLight2 = Color(0xFF959595);
  static const Color textFormField = Color(0xFFf7f7f7);
  static const Color primaryLight = Color(0xFFE8E8E8);
  static const Color primaryLight2 = Color(0xFFE2E6E7);
  static const Color backgroundSelectedBottomBarItem = Color(0xFFF1FBFF);
  static const Color textColor2 = Color(0xFF3D3D3D);
  static const Color subtitleColor = Color(0xff787878);
  static const Color subtitleColor2 = Color(0xffACACAC);
  static Color get white =>
      isDark ? const Color(0xFFF7F7F7) : const Color(0xFF3D3D3D);
  static Color get queanPage =>
      isDark ? const Color(0xFFF7F7F7) : const Color(0xFF435b79);
  static Color get black =>
      isDark ? const Color(0xFF3D3D3D) : const Color(0xFFF7F7F7);
  static const Color blackLight = Color(0xFF4B4B4B);
  static const Color success = Color(0xFF10A94B);
  static const Color red = Color(0xFFFF3B30);
  static const Color info = Color(0xFF296CAF);
  static const Color warning = Color(0xFFF7B313);
  static const Color greyLight = Color(0xFFB3B3B3);
  static const Color greyDark = Color(0xFF787878);
  static const Color greenLight = Color(0xFFF1FBFF);
  static const Color textFieldBorder = Color(0xFFCECECE);
  static const Color shadow = Color(0x26D6D6D6);
  static const Color emptyStar = Color(0xFFF1F1F1);
  static const Color green = Color(0xFF30D158);
  static const Color scaffoldBackgroundColor = Color(0xFFFfffff);

  static MaterialColor get primarySwatch =>
      const MaterialColor(0xffB97E12, <int, Color>{
        50: Color(0xffFDF5E0),
        100: Color(0xffFBEDC1),
        200: Color(0xffFAE5A2),
        300: Color(0xffF9DD83),
        400: Color(0xffF7D664),
        500: Color(0xffB97E12),
        600: Color(0xffA37111),
        700: Color(0xff8B6410),
        800: Color(0xff73570F),
        900: Color(0xff5A490D),
      });
}
