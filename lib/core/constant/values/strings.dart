import 'package:easy_localization/easy_localization.dart';

abstract class AppStrings {
  static const String _update = 'update';
  static String get update => _update.tr();

  static const String _updateAppTitle = 'updateAppTitle';
  static String get updateAppTitle => _updateAppTitle.tr();

  static const String _updateAppBody = 'updateAppBody';
  static String get updateAppBody => _updateAppBody.tr();
}
