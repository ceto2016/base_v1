import 'dart:developer';

extension DoubleToFraction on double {
  String get getRob3 {
    const tolerance = 1.0e-10;
    int whole = floor();
    double fraction = this - whole;

    if (fraction < tolerance) {
      return whole.toString();
    }

    for (int denominator = 2; denominator < 10000; denominator++) {
      if ((fraction * denominator) % 1 < tolerance) {
        int numerator = (fraction * denominator).round();
        return '$numerator';
      }
    }

    return toString(); // Return original value if no exact fraction found
  }

  String get getHezb {
    log(toString());
    final newHezb = toInt();
    return newHezb.isEven ? '2' : '1';
  }
}
