import 'package:flutter/material.dart';

import '../values/colors.dart';
import '../values/fonts.dart';
import '../values/size_config.dart';
import '../values/text_styles.dart';

class MaterialAppTheme {
  static AppBarTheme appBarTheme(bool isDarkMode) => AppBarTheme(
        toolbarHeight: kToolbarHeight,
        backgroundColor:
            isDarkMode ? AppColors.bgColor : AppColors.scaffoldBackgroundColor,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadiusDirectional.only(
            bottomStart: Radius.circular(SizeConfig.radius),
            bottomEnd: Radius.circular(SizeConfig.radius),
          ),
        ),
        iconTheme: IconThemeData(color: AppColors.white),
        foregroundColor: AppColors.white,
        actionsIconTheme: IconThemeData(
          color: AppColors.primary,
        ),
        titleTextStyle: TextStyles.bold20(),
      );

  static ElevatedButtonThemeData elevatedButtonTheme(bool isDarkMode) =>
      ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          backgroundColor:
              isDarkMode ? AppColors.primary : AppColors.buttonColor,
          textStyle: TextStyles.bold20(),
          elevation: 0.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SizeConfig.radius * 0.5),
          ),
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.hPadding * 3,
            vertical: SizeConfig.vPadding,
          ),
        ),
      );

  static OutlinedButtonThemeData outlinedButtonTheme(bool isDarkMode) =>
      OutlinedButtonThemeData(
        style: OutlinedButton.styleFrom(
          foregroundColor: AppColors.primary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SizeConfig.radius * 0.5),
          ),
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.hPadding * 2,
            vertical: SizeConfig.vPadding,
          ),
          side: BorderSide(color: AppColors.primary),
        ),
      );

  static DropdownMenuThemeData dropdownMenuThemeData(bool isDarkMode) =>
      DropdownMenuThemeData(
        inputDecorationTheme: InputDecorationTheme(
          border: OutlineInputBorder(
            borderSide: const BorderSide(color: AppColors.greyDark),
            borderRadius: BorderRadius.circular(SizeConfig.radius * 0.5),
          ),
          contentPadding: EdgeInsets.symmetric(
            horizontal: SizeConfig.hPadding,
            vertical: SizeConfig.vPadding,
          ),
        ),
      );

  static ThemeData mainThemeData(bool isDarkMode) {
    return ThemeData(
      useMaterial3: false,
      bottomSheetTheme: BottomSheetThemeData(
        backgroundColor: AppColors.bgColor,
      ),
      brightness: isDarkMode ? Brightness.dark : Brightness.light,
      scaffoldBackgroundColor:
          isDarkMode ? AppColors.bgColor : AppColors.scaffoldBackgroundColor,
      inputDecorationTheme: InputDecorationTheme(
        border: InputBorder.none,
        filled: true,
        fillColor: isDarkMode
            ? AppColors.cardColor
            : AppColors.scaffoldBackgroundColor,
        labelStyle: TextStyles.regular16(color: AppColors.textColor),
        contentPadding: EdgeInsets.symmetric(
          horizontal: SizeConfig.hPadding,
          vertical: SizeConfig.vPadding,
        ),
      ),
      dropdownMenuTheme: dropdownMenuThemeData(isDarkMode),
      fontFamily: AppFonts.dubai,
      // primaryColor: AppColors.primary,
      appBarTheme: appBarTheme(isDarkMode),
      outlinedButtonTheme: outlinedButtonTheme(isDarkMode),
      elevatedButtonTheme: elevatedButtonTheme(isDarkMode),
    );
  }

  static InputDecoration get inputDecoration => InputDecoration(
        filled: true,
        errorMaxLines: 2,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: BorderSide(
            color: AppColors.cardColor,
            width: 1,
          ),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: BorderSide(
            color: AppColors.cardColor,
            width: 1,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: BorderSide(
            color: AppColors.cardColor,
            width: 1,
          ),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(
            color: AppColors.cardColor,
            width: 1.0,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        focusedErrorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(
              color: AppColors.red,
              width: 1,
              style: BorderStyle.solid,
            )),
      );
}
