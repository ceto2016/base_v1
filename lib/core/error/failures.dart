import 'package:equatable/equatable.dart';

import '../../features/auth/domain/entities/user.dart';

abstract class Failure extends Equatable {
  final String message;

  const Failure({required this.message});
  @override
  List<Object> get props => <Object>[];
}

class NotActiveFailure extends Failure {
  final User user;
  const NotActiveFailure({
    required super.message,
    required this.user,
  });
}

// General failures
class ServerFailure extends Failure {
  final int? code;

  const ServerFailure({
    super.message = '',
    this.code,
  });
}

class CacheFailure extends Failure {
  const CacheFailure({required super.message});
}
