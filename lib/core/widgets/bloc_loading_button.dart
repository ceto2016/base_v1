import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../constant/values/colors.dart';
import '../constant/values/text_styles.dart';

class LoaddingEButton<C extends Cubit<S>, S, L> extends StatelessWidget {
  const LoaddingEButton({
    super.key,
    required this.onPressed,
    required this.title,
  });
  final void Function() onPressed;
  final String title;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<C, S>(
      builder: (BuildContext context, S state) {
        return state is L
            ? Center(
                child: CircularProgressIndicator(
                  color: AppColors.primary,
                ),
              )
            : ElevatedButton(
                onPressed: onPressed,
                child:
                    Text(title, style: TextStyles.bold16(color: Colors.white)));
      },
    );
  }
}

class LoadingOButton<C extends Cubit<S>, S, L> extends StatelessWidget {
  const LoadingOButton({
    super.key,
    required this.onTap,
    required this.title,
    required this.textColor,
    this.style,
  });
  final void Function() onTap;
  final String title;
  final ButtonStyle? style;
  final Color? textColor;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<C, S>(
      builder: (BuildContext context, S state) {
        return state is L
            ? Center(
                child: CircularProgressIndicator(
                  color: AppColors.primary,
                ),
              )
            : OutlinedButton(
                style: style,
                onPressed: onTap,
                child: Text(title,
                    style:
                        TextStyles.bold16(color: textColor ?? Colors.white)));
      },
    );
  }
}
