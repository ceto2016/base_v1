import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

extension AppSpacer on int {
  Widget get sh => SizedBox(height: h);
  Widget get sw => SizedBox(height: w);
}
