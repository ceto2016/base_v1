import 'package:intl_phone_number_input/intl_phone_number_input.dart';

class IntiPhoneNumber extends PhoneNumber {
  toMap() {
    String phoneNumberTemp = phoneNumber ?? '';
    phoneNumberTemp = phoneNumberTemp.contains('+')
        ? phoneNumberTemp.substring(dialCode!.length)
        : phoneNumberTemp;
    phoneNumberTemp = phoneNumberTemp.startsWith('0')
        ? phoneNumberTemp.substring(1)
        : phoneNumberTemp;
    return <String, String?>{
      'code': dialCode,
      'flag': isoCode,
      'mobile': phoneNumberTemp
    };
  }

  @override
  String toString() {
    return '${dialCode!}${phoneNumber!}';
  }

  IntiPhoneNumber({
    required super.isoCode,
    required super.dialCode,
    required super.phoneNumber,
  });
  factory IntiPhoneNumber.fromPN(PhoneNumber ph) {
    return IntiPhoneNumber(
        isoCode: ph.isoCode,
        dialCode: ph.dialCode,
        phoneNumber: (ph.phoneNumber?.startsWith('+') ?? false)
            ? ph.phoneNumber?.substring(ph.dialCode?.length ?? 0) ?? ''
            : ph.phoneNumber);
  }
  factory IntiPhoneNumber.fromMap(Map<String, dynamic> map) {
    return IntiPhoneNumber(
      dialCode: map['code'],
      isoCode: map['flag'],
      phoneNumber: map['mobile'],
    );
  }
}
