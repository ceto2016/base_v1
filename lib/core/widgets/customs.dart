import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../constant/values/colors.dart';

class Customs {
  static Future<dynamic> customBottomSheet({
    required BuildContext context,
    required bool isScrollControlled,
    required Widget child,
  }) {
    return showModalBottomSheet(
      backgroundColor: AppColors.bgColor,
      isScrollControlled: isScrollControlled,
      enableDrag: true,
      showDragHandle: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.h),
          topRight: Radius.circular(20.h),
        ),
      ),
      context: context,
      builder: (BuildContext context) {
        return Container(
          // margin: EdgeInsets.only(top: 50.h),
          padding: EdgeInsets.symmetric(horizontal: 24.h, vertical: 10.h),
          child: child,
        );
      },
    );
  }

  static Future<void> showAppDialog(BuildContext context, Widget child) async {
    await showAdaptiveDialog(
        context: context,
        barrierDismissible: true,
        builder: (
          BuildContext context,
        ) =>
            Container(
              padding: EdgeInsets.symmetric(horizontal: 24.w),
              child: Material(
                  type: MaterialType.transparency,
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12.r),
                      ),
                      padding: EdgeInsets.symmetric(
                          horizontal: 16.w, vertical: 20.h),
                      child: child)),
            ));

  }
}
