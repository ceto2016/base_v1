import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../constant/values/assets.dart';
import '../constant/values/text_styles.dart';

class NoResultsWidget extends StatelessWidget {
  const NoResultsWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            AppAssets.imagesNoResults,
          ),
          SizedBox(
            height: 16.h,
          ),
          Text(
            'no_results_found'.tr(),
            style: TextStyles.bold20(),
          ),
        ],
      ),
    );
  }
}