import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../features/auth/presentation/cubit/auto_login/auto_login_cubit.dart';

class HideFromGuestWidget extends StatelessWidget {
  const HideFromGuestWidget({super.key, required this.child, this.guestWidget});
  final Widget child;
  final Widget? guestWidget;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AutoLoginCubit, AutoLoginState>(
      builder: (context, state) {
        if (state is AutoLoginGuest) {
          return guestWidget ?? const SizedBox();
        }
        return child;
      },
    );
  }
}

showLoginAlert(BuildContext context) {
  showDialog(
    context: context,
    builder: (context) => const AlertDialog(),
  );
}
