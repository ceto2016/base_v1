import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../constant/values/assets.dart';

class AppLogo extends StatelessWidget {
  final double width, height;
  final BoxFit? fit;
  final bool isSvg;

  const AppLogo({
    this.width = 120.0,
    this.height = 120.0,
    this.fit = BoxFit.fill,
    this.isSvg = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (BuildContext context) {
        if(isSvg){
          return SvgPicture.asset(
            AppAssets.imagesLogoSvg,
            allowDrawingOutsideViewBox: true,
            width: width.w,
            height: height.h,
            fit: fit?? BoxFit.fill,
          );
        }
        return Image.asset(
          AppAssets.imagesLogo,
          width: width.w,
          height: height.h,
          fit: fit?? BoxFit.fill,
        );
      }
    );
  }
}
