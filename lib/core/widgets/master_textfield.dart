// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import '../constant/values/colors.dart';
import '../constant/values/text_styles.dart';
import '../util/validator.dart';

class MasterTextField extends StatefulWidget {
  final double? sidePadding;
  final double? fieldHeight;
  final double? fieldWidth;
  final double? iconHeight;
  final bool? readOnly;
  final double? suffixIconHeight;
  final TextEditingController? controller;
  final Color? hintColor;
  final TextInputType? keyboardType;
  final bool? isPassword;
  final bool? autofocus;
  final Color? borderColor;
  final Color? textFieldColor;
  final String? errorText;
  final String? hintText;
  final TextStyle? hintStyle;
  final double? elevation;
  final List<BoxShadow>? boxShadow;
  final Color? shadowColor;
  final bool? enabled;
  final double? borderRadius;
  final IconData? prefixIcon;
  final String? suffixText;
  final IconData? suffixIcon;
//final List<TextInputFormatter>? inputFormatters;
  final Color? suffixColor;
  final Color? prefixColor;
  final int? maxLines;
  final int? minLines;
  final Function(String)? onChanged;
  final int? borderWidth;
  final Function(String)? onSubmit;
  final Function()? onTap;
  final String? Function(String?) validate;
  final Widget? suffix;
  final Color? fillColor;
  final TextAlign? textAlign;
  final Widget? suffixWidget;
  final void Function(String?)? onSaved;
  final TextDirection? textDirection;
  const MasterTextField({
    Key? key,
    // this.inputFormatters,
    this.sidePadding,
    this.fieldHeight,
    this.fieldWidth,
    this.iconHeight,
    this.readOnly,
    this.suffixIconHeight,
    this.controller,
    this.hintColor,
    this.keyboardType,
    this.isPassword,
    this.autofocus,
    this.borderColor,
    this.textFieldColor,
    this.errorText,
    this.hintText,
    this.hintStyle,
    this.elevation,
    this.boxShadow,
    this.shadowColor,
    this.enabled,
    this.borderRadius,
    this.prefixIcon,
    this.suffixText,
    this.suffixIcon,
    this.suffixColor,
    this.prefixColor,
    this.maxLines,
    this.minLines,
    this.onChanged,
    this.borderWidth,
    this.onSubmit,
    this.onTap,
    this.validate = Validator.defaultEmptyValidator,
    this.suffix,
    this.fillColor,
    this.textAlign,
    this.suffixWidget,
    this.onSaved,
    this.textDirection = TextDirection.rtl,
  }) : super(key: key);

  @override
  State<MasterTextField> createState() => _MasterTextFieldState();
}

class _MasterTextFieldState extends State<MasterTextField> {
  bool secure = false;
  TextDirection? textDirection;
  String? fontFamily;
  @override
  void initState() {
    super.initState();
    secure = widget.isPassword ?? false;
    textDirection = widget.textDirection;
    if (widget.keyboardType == TextInputType.number) {
      fontFamily = 'Avenir';
    }
    widget.controller?.addListener(() {
      // _checkForArabicLetter(widget.controller?.text ?? '');
    });
  }

  @override
  void didUpdateWidget(covariant MasterTextField oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {});
  }

  // void _checkForArabicLetter(String text) {
  //   final RegExp arabicRegex = RegExp(r'[ء-ي-_ \.]*$');
  //   final RegExp englishRegex = RegExp(r'[a-zA-Z ]');
  //   final RegExp spi = RegExp("[\$&+,:;=?@#|'<>.^*()%!-]+");
  //   final RegExp numbers = RegExp('^[0-9]*\$');
  //   setState(() {
  //     text.contains(arabicRegex) &&
  //             !text.startsWith(englishRegex) &&
  //             !text.startsWith(spi) &&
  //             !text.startsWith(numbers)
  //         ? textDirection = TextDirection.rtl
  //         : textDirection = TextDirection.ltr;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          // boxShadow: widget.boxShadow ??
          //     <BoxShadow>[
          //       BoxShadow(
          //color: Colors.grey.withOpacity(0.2), // Shadow color
          //         spreadRadius: 1, // Spread radius
          //         blurRadius: 5, // Blur radius
          //         offset: const Offset(0, 2), // Offset in x and y direction
          //       ),
          //     ],
          ),
      child: TextFormField(
        readOnly: widget.readOnly ?? false,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        //autocorrect: true,
        controller: widget.controller,
        onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
        onChanged: (String value) {
          // if (mounted) {
          //   _checkForArabicLetter(value);
          // }
          if (widget.onChanged != null) widget.onChanged!(value);
        },
        onTap: () {
          if (widget.onTap != null) {
            widget.onTap!();
          }
          if (widget.controller!.selection ==
              TextSelection.fromPosition(
                  TextPosition(offset: widget.controller!.text.length - 1))) {
            setState(() {
              widget.controller!.selection = TextSelection.fromPosition(
                  TextPosition(offset: widget.controller!.text.length));
            });
          }
        },
        keyboardType: widget.keyboardType,

        obscureText: secure,
        maxLines: widget.maxLines ?? 1,
        minLines: widget.minLines ?? 1,
        autofocus: widget.autofocus ?? false,
        style: TextStyles.regular16()
            .copyWith(color: AppColors.inputFieldTextColor),
        enabled: widget.enabled,
        onSaved: widget.onSaved,
        validator: widget.validate,

        onFieldSubmitted: widget.onSubmit,
        textDirection: textDirection,
        // inputFormatters: inputFormatters,
        decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
          filled: true,
          fillColor: widget.fillColor ?? AppColors.cardColor,
          errorMaxLines: 2,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(widget.borderRadius ?? 10.0),
            borderSide: BorderSide(
              color: widget.borderColor ?? AppColors.cardColor,
              width: 1,
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(widget.borderRadius ?? 10.0),
            borderSide: BorderSide(
              color: widget.borderColor ?? AppColors.cardColor,
              width: 1,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(widget.borderRadius ?? 10.0),
            borderSide: BorderSide(
              color: widget.borderColor ?? AppColors.cardColor,
              width: 1,
            ),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: widget.borderColor ?? AppColors.cardColor,
              width: 1.0,
            ),
            borderRadius: BorderRadius.circular(widget.borderRadius ?? 10),
          ),
          focusedErrorBorder: OutlineInputBorder(
              borderRadius:
                  BorderRadius.all(Radius.circular(widget.borderRadius ?? 10)),
              borderSide: BorderSide(
                color: widget.errorText != null
                    ? AppColors.red
                    : AppColors.textColor,
                width: 1,
                style: BorderStyle.solid,
              )),
          hintText: widget.hintText,
          hintStyle: TextStyles.regular16()
              .copyWith(color: AppColors.inputFieldTextColor),
          suffixIconColor: widget.suffixColor ?? AppColors.inputFieldTextColor,
          prefixIconColor: widget.prefixColor ?? AppColors.inputFieldTextColor,
          suffix: widget.suffix,
          prefixIcon: widget.prefixIcon == null
              ? null
              : Icon(
                  widget.prefixIcon,
                  color: widget.prefixColor ??
                      AppColors.inputFieldTextColor.withOpacity(.8),
                ),
          suffixIcon: widget.suffixWidget ??
              (widget.isPassword ?? false
                  ? IconButton(
                      onPressed: () => setState(() {
                            secure = !secure;
                          }),
                      icon: Icon(
                        !secure ? Icons.visibility : Icons.visibility_off,
                        color: AppColors.inputFieldTextColor.withOpacity(.8),
                      ))
                  : widget.suffixIcon == null
                      ? null
                      : Icon(
                          widget.suffixIcon,
                          color: widget.suffixColor ??
                              AppColors.inputFieldTextColor.withOpacity(.8),
                        )),
        ),
      ),
    );
  }
}
