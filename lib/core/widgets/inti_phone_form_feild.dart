import 'dart:developer';

import 'package:easy_localization/easy_localization.dart' as e;
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

import '../constant/values/colors.dart';
import '../constant/values/text_styles.dart';
import '../util/validator.dart';
import 'init_phone.dart';
// import '../../features/auth/domain/entities/rawy_phone_number.dart';
// import '../constant/colors/colors.dart';
// import '../util/validator.dart';
// import 'space_between_ele.dart';

class PhoneFormFeild extends StatefulWidget {
  // final TextEditingController controller;
  final bool obSecure;
  final bool isClickable;
  final bool editProfile;
  final TextInputType keyboardType;
  final String? hintText;
  final Function(String)? onChanged;
  final Function? validateFunction;
  final TextEditingController controller;
  final Function(IntiPhoneNumber) selectedPhoneCountryFunc;
  final IntiPhoneNumber? initValue;
  const PhoneFormFeild({
    Key? key,
    required this.controller,
    this.isClickable = true,
    this.obSecure = false,
    this.editProfile = false,
    this.initValue,
    this.keyboardType = TextInputType.text,
    this.validateFunction,
    this.hintText,
    this.onChanged,
    required this.selectedPhoneCountryFunc,
  }) : super(key: key);

  @override
  State<PhoneFormFeild> createState() => _PhoneFormFeildState();
}

class _PhoneFormFeildState extends State<PhoneFormFeild> {
  bool secure = false;
  TextDirection? textDirection;
  String? fontFamily;
  @override
  void initState() {
    super.initState();
    if (widget.keyboardType == TextInputType.number) {
      fontFamily = '';
    }
    if (!widget.editProfile) {
      widget
          .selectedPhoneCountryFunc(IntiPhoneNumber.fromPN(selectedPhoneModel));
    } else {
      if (widget.initValue != null) {
        selectedPhoneModel = PhoneNumber(
          phoneNumber: widget.initValue!.phoneNumber,
          isoCode: widget.initValue!.isoCode,
          dialCode: widget.initValue!.dialCode,
        );
      }
    }
  }

  @override
  void didUpdateWidget(covariant PhoneFormFeild oldWidget) {
    super.didUpdateWidget(oldWidget);
    setState(() {});
  }

  PhoneNumber selectedPhoneModel = PhoneNumber(
    phoneNumber: '',
    isoCode: 'SA',
    dialCode: '+966',
  );
  TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    log(selectedPhoneModel.toString());
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        // Text(e.tr("phone"), style: TextStyles.textViewMedium16),
        // const SpaceV5BE(),
        Directionality(
          textDirection: TextDirection.ltr,
          child: InternationalPhoneNumberInput(
            initialValue: selectedPhoneModel,
            textFieldController: controller,
            onInputChanged: (PhoneNumber number) {
              widget.selectedPhoneCountryFunc(IntiPhoneNumber.fromPN(number));
              if (selectedPhoneModel.isoCode != number.isoCode) {
                selectedPhoneModel = number;
                controller.clear();
                setState(() {});
              }
            },
            isEnabled: widget.isClickable,
            onInputValidated: (bool status) {},
            spaceBetweenSelectorAndTextField: 4,
            errorMessage: e.tr('error_wrong_input'),
            selectorConfig: const SelectorConfig(
                setSelectorButtonAsPrefixIcon: true,
                leadingPadding: 16,
                trailingSpace: false,
                selectorType: PhoneInputSelectorType.BOTTOM_SHEET),
            ignoreBlank: true,
            autoValidateMode: AutovalidateMode.onUserInteraction,
            selectorTextStyle: TextStyles.medium16(),
            textStyle: TextStyles.medium16(),
            formatInput: true,
            inputDecoration: InputDecoration(
              hintText: selectedPhoneModel.isoCode == 'SA' ? '5xxxxxxxx' : null,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(
                  color: AppColors.cardColor,
                  width: 1,
                ),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(
                  color: AppColors.cardColor,
                  width: 1,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(
                  color: AppColors.cardColor,
                  width: 1,
                ),
              ),
              border: OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColors.cardColor,
                  width: 1.0,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
              focusedErrorBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(
                    color: AppColors.red,
                    width: 1,
                    style: BorderStyle.solid,
                  )),
            ),
            hintText: selectedPhoneModel.isoCode == 'SA' ? '5xxxxxxxx' : null,
            validator: widget.editProfile
                ? null
                : selectedPhoneModel.isoCode == 'SA'
                    ? (String? val) => Validator.phone(
                        val,
                        const PhoneModel(
                            code: '+966', lenght: 9, startWith: '5'))
                    : null,
            keyboardType: TextInputType.phone,
            onSaved: (PhoneNumber number) {
              // widget.selectedPhoneCountryFunc(number);
              // selectedPhoneModel = number;
              // setState(() {});
            },
          ),
        ),
      ],
    );
  }
}

class PhoneModel extends Equatable {
  final String code;
  final String startWith;
  final int lenght;

  const PhoneModel(
      {required this.code, required this.lenght, required this.startWith});

  @override
  List<Object?> get props => <Object?>[code, lenght];
}
