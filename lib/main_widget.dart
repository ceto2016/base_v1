import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
// ignore: depend_on_referenced_packages
import 'package:nested/nested.dart';

import 'app.dart';
import 'core/constant/styles/material_app_theme.dart';
import 'core/constant/values/size_config.dart';
import 'features/auth/auth_injection.dart';
import 'features/books/books_injection.dart';
import 'features/more/more_inj.dart';
import 'injection_container.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        useInheritedMediaQuery: true,
        designSize: const Size(430, 971),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (BuildContext context, Widget? child) {
          SizeConfig().init(context);
          return MultiBlocProvider(
              providers: <SingleChildWidget>[
                ...authBlocs(context),
                ...moreBlocs(context),
                ...booksBlocs(context),
              ],
              child: MaterialApp(
                debugShowCheckedModeBanner: false,
                title: 'AwamerBase',
                theme: MaterialAppTheme.mainThemeData(false),
                locale: context.locale,
                localizationsDelegates: context.localizationDelegates,
                supportedLocales: context.supportedLocales,
                navigatorKey: appNavigator.navigatorKey,
                home: const AwamerBaseApp(),
              ));
        });
  }
}
