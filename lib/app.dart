import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'core/util/api_basehelper.dart';
import 'features/auth/presentation/cubit/auto_login/auto_login_cubit.dart';
import 'injection_container.dart' as di;

class AwamerBaseApp extends StatefulWidget {
  const AwamerBaseApp({super.key});

  @override
  State<AwamerBaseApp> createState() => _AwamerBaseAppState();
}

class _AwamerBaseAppState extends State<AwamerBaseApp> {
  @override
  void initState() {
    super.initState();
    Future<void>.microtask(() {
      final String local =
          EasyLocalization.of(context)!.currentLocale!.languageCode;
      di.sl<ApiBaseHelper>().updateLocalInHeaders(local);
      context.read<AutoLoginCubit>().fAutoLogin();
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AutoLoginCubit, AutoLoginState>(
      builder: (BuildContext context, AutoLoginState state) {
        if (state is AutoLoginHasUser || state is AutoLoginGuest) {
          return const Scaffold();
        } else if (state is AutoLoginSeenIntro) {
          return const Scaffold();
        } else if (state is AutoLoginNoUser) {
          return const Scaffold();
        } else if (state is PickLanguage || state is PickLanguageLoading) {
          return const Scaffold();
        } else {
          return const Scaffold();
        }
      },
    );
  }
}
