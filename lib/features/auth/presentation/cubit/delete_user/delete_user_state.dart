part of 'delete_user_cubit.dart';

class DeleteUserState extends Equatable {
  const DeleteUserState();

  @override
  List<Object> get props => <Object>[];
}

class DeleteUserInitial extends DeleteUserState {}

class DeleteUserSuccess extends DeleteUserState {
  final String message;

  const DeleteUserSuccess({required this.message});
}

class DeleteUserError extends DeleteUserState {
  final String message;

  const DeleteUserError({required this.message});
}

class DeleteUserLoading extends DeleteUserState {}
