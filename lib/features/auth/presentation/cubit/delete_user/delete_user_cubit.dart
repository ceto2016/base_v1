// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/widgets/toast.dart';
import '../../../../../injection_container.dart';
import '../../../domain/usecases/delete_account_usecase.dart';
import '../auto_login/auto_login_cubit.dart';
import '../logout/logout_cubit.dart';

part 'delete_user_state.dart';

class DeleteUserCubit extends Cubit<DeleteUserState> {
  DeleteUserCubit({
    required this.deleteUser,
  }) : super(DeleteUserInitial());
  final DeleteUserUseCase deleteUser;

  Future<void> fDeleteUser() async {
    emit(DeleteUserLoading());

    final Either<Failure, String> failOrUser = await deleteUser(DeleteParams());
    failOrUser.fold((Failure fail) {
      String message = 'please try again later';
      if (fail is ServerFailure) {
        message = fail.message;
        showToast(message);
      }
      emit(DeleteUserError(message: message));
    }, (String logout) {
      appNavigator.popToFrist();
      sl<AutoLoginCubit>().emitSeenIntro();
      sl<LogoutCubit>().fLogout();
      emit(DeleteUserSuccess(message: logout));
    });
  }
}
