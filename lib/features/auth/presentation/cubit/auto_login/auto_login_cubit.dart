import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/usecases/usecases.dart';
import '../../../../../core/widgets/hide_from_guest_widget.dart';
import '../../../../../injection_container.dart';
import '../../../domain/entities/user.dart';
import '../../../domain/usecases/auto_login.dart';
import '../login/login_cubit_cubit.dart';

part 'auto_login_state.dart';

class AutoLoginCubit extends Cubit<AutoLoginState> {
  AutoLoginCubit({required this.autoLogin}) : super(AutoLoginInitial());
  final AutoLoginUsecase autoLogin;
  Future<void> fAutoLogin() async {
    emit(AutoLoginLoading());
    final Either<Failure, User> failOrUser = await autoLogin(NoParams());
    await initAppData();
    failOrUser.fold((Failure fail) async {
      await Future.delayed(const Duration(seconds: 3));
      emit(PickLanguage());
    }, (User user) async {
      await Future.delayed(const Duration(seconds: 3));
      sl<LoginCubit>().updateUser = user;
      emit(AutoLoginHasUser(user: user));
    });
  }

  initAppData() async {
    await Future.wait([]);
  }

  int selectedIndex = 0;
  void emitLoading() {
    emit(AutoLoginLoading());
  }

  emitHasUser({required User user}) {
    emit(AutoLoginHasUser(user: user));
  }

  void emitSeenIntro() {
    emit(AutoLoginSeenIntro());
  }

  emitNoUser() {
    emit(AutoLoginNoUser());
  }

  Future<void> emitChangeLang() async {
    final AutoLoginState lastState = state;
    emit(PickLanguageLoading());
    await Future.wait([
      Future.delayed(const Duration(seconds: 3)),
      // if (isUser) sl<GetProfileCubit>().fGetProfile(),
    ]);
    emit(lastState);
  }

  bool get isGuest => state is AutoLoginGuest;
  bool get isUser => state is AutoLoginHasUser;
  emitGuest() {
    emit(AutoLoginGuest());
  }

  void functionOrLoginAlert(
      {required BuildContext context, required Function function}) {
    if (isGuest) {
      showLoginAlert(context);
    } else {
      function();
    }
  }
}
