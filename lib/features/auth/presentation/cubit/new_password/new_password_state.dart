import 'package:equatable/equatable.dart';

abstract class CreateNewPasswordState extends Equatable {
  const CreateNewPasswordState();

  @override
  List<Object> get props => <Object>[];
}

class CreateNewPasswordInitial extends CreateNewPasswordState {}

class CreateNewPasswordSuccess extends CreateNewPasswordState {}

class CreateNewPasswordError extends CreateNewPasswordState {
  final String message;

  const CreateNewPasswordError({required this.message});
}

class CreateNewPasswordLoading extends CreateNewPasswordState {}
