import 'package:dartz/dartz.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../core/error/failures.dart';
import '../../../../../core/widgets/toast.dart';
import '../../../../../injection_container.dart';
import '../../../domain/entities/create_new_pass.dart';
import '../../../domain/usecases/create_new_pass.dart';
import 'new_password_state.dart';

class CreateNewPasswordCubit extends Cubit<CreateNewPasswordState> {
  CreateNewPasswordCubit({required this.createNewPasswordUseCase})
      : super(CreateNewPasswordInitial());
  final CreateNewPasswordUseCase createNewPasswordUseCase;
  Future<void> fCreateNewPassword({
    required String newPass,
    required String confirmPass,
  }) async {
    emit(CreateNewPasswordLoading());

    final Either<Failure, CreateNewPassResponse> response =
        await createNewPasswordUseCase(CreateNewPasswordParams(
      pass: newPass,
      passConfirm: confirmPass,
    ));
    response.fold((Failure fail) async {
      if (fail is ServerFailure) {
        String message = fail.message;
        showToast(message);
        emit(CreateNewPasswordError(message: message));
      }
    }, (CreateNewPassResponse info) {
      appNavigator.popToFrist();
      showSuccessToast('pass_changed_successfully'.tr());
      emit(CreateNewPasswordSuccess());
    });
  }
}
