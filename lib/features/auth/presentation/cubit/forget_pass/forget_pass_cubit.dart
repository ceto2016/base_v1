import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../core/error/failures.dart';
import '../../../../../core/widgets/init_phone.dart';
import '../../../../../core/widgets/toast.dart';
import '../../../domain/entities/forget_pass.dart';
import '../../../domain/usecases/forget_pass.dart';

part 'forget_pass_state.dart';

class ForgetPassCubit extends Cubit<ForgetPassState> {
  ForgetPassCubit(this.usecase) : super(ForgetPassInitial());

  final ForgetPasswordUseCase usecase;
  Future<void> fForgetPassword({required IntiPhoneNumber phone}) async {
    emit(ForgetPasswordLoading());

    final Either<Failure, ForgetPassResponse> response =
        await usecase(ForgetPasswordParams(phone: phone));
    response.fold((Failure fail) async {
      if (fail is ServerFailure) {
        String message = fail.message;
        showErrorToast(message);
        emit(ForgetPasswordError(message: message));
      }
    }, (ForgetPassResponse info) {
      emit(ForgetPasswordSuccess(response: info));
    });
  }
}
