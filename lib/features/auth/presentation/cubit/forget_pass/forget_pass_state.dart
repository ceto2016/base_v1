part of 'forget_pass_cubit.dart';

class ForgetPassState extends Equatable {
  const ForgetPassState();

  @override
  List<Object> get props => <Object>[];
}

class ForgetPassInitial extends ForgetPassState {}

class ForgetPasswordSuccess extends ForgetPassState {
  final ForgetPassResponse response;

  const ForgetPasswordSuccess({required this.response});
}

class ForgetPasswordError extends ForgetPassState {
  final String message;

  const ForgetPasswordError({required this.message});
}

class ForgetPasswordLoading extends ForgetPassState {}
