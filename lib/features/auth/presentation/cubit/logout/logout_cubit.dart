// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/usecases/usecases.dart';
import '../../../../../core/widgets/toast.dart';
import '../../../../../injection_container.dart';
import '../../../domain/usecases/logout_usecase.dart';
import '../auto_login/auto_login_cubit.dart';
import '../login/login_cubit_cubit.dart';

part 'logout_state.dart';

class LogoutCubit extends Cubit<LogoutState> {
  LogoutCubit({
    required this.logout,
  }) : super(LogoutInitial());

  final LogoutUseCase logout;

  Future<void> fLogout() async {
    emit(LogoutLoading());
    final Either<Failure, String> failOrUser = await logout(NoParams());
    failOrUser.fold((Failure fail) {
      String message = 'please try again later';
      if (fail is ServerFailure) {
        message = fail.message;
        showToast(message);
      }
      emit(LogoutError(message: message));
    }, (String logout) {
      appNavigator.popToFrist();
      sl<AutoLoginCubit>().emitNoUser();
      sl<LoginCubit>().logOut();
      emit(LogoutSuccess(message: logout));
    });
  }
}
