import 'package:equatable/equatable.dart';

import '../../../domain/entities/register_response.dart';

abstract class RegisterState extends Equatable {
  const RegisterState();

  @override
  List<Object> get props => <Object>[];
}

class AuthInitial extends RegisterState {}

class RegisterLoodingState extends RegisterState {}

class RegisterSuccessState extends RegisterState {
  final RegisterResponse response;
  const RegisterSuccessState({
    required this.response,
  });
}

class RegisterErrorState extends RegisterState {
  final String message;
  const RegisterErrorState({required this.message});
}
