import 'package:dartz/dartz.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/widgets/toast.dart';
import '../../../../../injection_container.dart';
import '../../../domain/entities/register_response.dart';
import '../../../domain/usecases/register.dart';
import '../auto_login/auto_login_cubit.dart';
import '../login/login_cubit_cubit.dart';
import 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit({required this.usecase}) : super(AuthInitial());
  RegisterUsecase usecase;
  Future<void> fRegister({
    required GlobalKey<FormState> formKey,
    required BuildContext context,
    required String name,
    required String email,
    required String flag,
    required String code,
    required String phone,
    required String password,
    required String passwordConfirm,
    required String terms,
  }) async {
    if (!formKey.currentState!.validate()) {
      return;
    } else {
      emit(RegisterLoodingState());
      final Either<Failure, RegisterResponse> failOrUser =
          await usecase(RegisterParams(
        name: name,
        flag: flag,
        code: code,
        email: email,
        mobile: phone,
        password: password,
        passwordConfirmation: passwordConfirm,
        terms: terms,
      ));
      failOrUser.fold((Failure fail) {
        if (fail is ServerFailure) {
          emit(RegisterErrorState(message: fail.message));
        }
      }, (RegisterResponse response) {
        showSuccessToast('register_success'.tr());
        appNavigator.popToFrist();
        sl<LoginCubit>().updateUser = response.data.user;
        sl<AutoLoginCubit>().emitHasUser(user: response.data.user);
        emit(RegisterSuccessState(response: response));
      });
    }
  }
}
