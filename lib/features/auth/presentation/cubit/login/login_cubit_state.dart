part of 'login_cubit_cubit.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => <Object>[];
}

class LoginCubitInitial extends LoginState {}

class LoginLoadingState extends LoginState {}

class LoginSuccessState extends LoginState {
  final User user;

  const LoginSuccessState({required this.user});
  @override
  List<Object> get props => <Object>[user];
}

class LoginErrorState extends LoginState {
  final String message;
  const LoginErrorState({required this.message});
}
