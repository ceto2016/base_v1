import 'package:dartz/dartz.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/widgets/toast.dart';
import '../../../../../injection_container.dart';
import '../../../domain/entities/login_response.dart';
import '../../../domain/entities/user.dart';
import '../../../domain/usecases/login_usecase.dart';
import '../auto_login/auto_login_cubit.dart';

part 'login_cubit_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit(this.login) : super(LoginCubitInitial());
  final LoginUseCase login;
  User? _user;
  User get user => _user!;
  set updateUser(User newUser) {
    emit(LoginCubitInitial());
    _user = newUser;
    emit(LoginSuccessState(user: user));
  }

  Future<void> fUserLogin(
      {required String phone,
      required String flag,
      required String code,
      required String password}) async {
    emit(LoginLoadingState());

    final Either<Failure, LoginResponse> failOrUser = await login(LoginParams(
      password: password,
      phone: phone,
      flag: flag,
      code: code,
    ));
    failOrUser.fold((Failure fail) {
      showToast(fail.message);
      emit(LoginErrorState(message: fail.message));
    }, (LoginResponse user) {
      if (user.data.user.isVerified) {
        showErrorToast(tr('inactive_user'));
        emit(LoginCubitInitial());
        return;
      }
      _user = user.data.user;
      appNavigator.popToFrist();
      sl<AutoLoginCubit>().emitHasUser(user: _user!);
      emit(LoginSuccessState(user: user.data.user));
    });
  }

  void logOut() {
    _user = null;
    emit(LoginCubitInitial());
  }
}
