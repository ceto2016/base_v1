import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
// import '../entities/register_response.dart';
import '../entities/create_new_pass.dart';
import '../entities/forget_pass.dart';
import '../entities/login_response.dart';
import '../entities/register_response.dart';
import '../entities/user.dart';
import '../usecases/create_new_pass.dart';
import '../usecases/delete_account_usecase.dart';
import '../usecases/forget_pass.dart';
import '../usecases/login_usecase.dart';
import '../usecases/register.dart';

abstract class AuthRepository {
  Future<Either<Failure, RegisterResponse>> register(
      {required RegisterParams params});
  Future<Either<Failure, LoginResponse>> login({required LoginParams params});
  Future<Either<Failure, User>> autoLogin();
  Future<Either<Failure, String>> logout();
  Future<Either<Failure, ForgetPassResponse>> forgetPassword(
      {required ForgetPasswordParams params});
  Future<Either<Failure, CreateNewPassResponse>> createNewPass(
      {required CreateNewPasswordParams params});
  Future<Either<Failure, String>> deleteUser({required DeleteParams params});
}
