// To parse this JSON data, do
//
//     final createNewPassResponse = createNewPassResponseFromJson(jsonString);

import 'dart:convert';

CreateNewPassResponse createNewPassResponseFromJson(String str) =>
    CreateNewPassResponse.fromJson(json.decode(str));

String createNewPassResponseToJson(CreateNewPassResponse data) =>
    json.encode(data.toJson());

class CreateNewPassResponse {
  final String accessToken;

  CreateNewPassResponse({
    required this.accessToken,
  });

  factory CreateNewPassResponse.fromJson(Map<String, dynamic> json) =>
      CreateNewPassResponse(
        accessToken: json['access_token'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'access_token': accessToken,
      };
}
