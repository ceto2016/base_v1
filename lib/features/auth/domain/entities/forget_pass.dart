// To parse this JSON data, do
//
//     final forgetPassResponse = forgetPassResponseFromJson(jsonString);

import 'dart:convert';

import 'user.dart';

ForgetPassResponse forgetPassResponseFromJson(String str) =>
    ForgetPassResponse.fromJson(json.decode(str));

String forgetPassResponseToJson(ForgetPassResponse data) =>
    json.encode(data.toJson());

class ForgetPassResponse {
  final Data data;
  final int status;

  ForgetPassResponse({
    required this.data,
    required this.status,
  });

  factory ForgetPassResponse.fromJson(Map<String, dynamic> json) =>
      ForgetPassResponse(
        data: Data.fromJson(json['data']),
        status: json['status'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'data': data.toJson(),
        'status': status,
      };
}

class Data {
  final String verificationCode;
  final String accessToken;
  final User user;

  Data({
    required this.verificationCode,
    required this.accessToken,
    required this.user,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        verificationCode: json['verification_code'].toString(),
        accessToken: json['access_token'],
        user: User.fromJson(json['user']),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'verification_code': verificationCode,
        'access_token': accessToken,
        'user': user.toJson(),
      };
}
