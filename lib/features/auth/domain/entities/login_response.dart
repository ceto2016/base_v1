// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

import 'user.dart';

LoginResponse loginResponseFromJson(String str) =>
    LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  final Data data;
  final int status;

  LoginResponse({
    required this.data,
    required this.status,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        data: Data.fromJson(json['data']),
        status: json['status'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'data': data.toJson(),
        'status': status,
      };
}

class Data {
  final String accessToken;
  final User user;

  Data({
    required this.accessToken,
    required this.user,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        accessToken: json['access_token'],
        user: User.fromJson(json['user']),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'access_token': accessToken,
        'user': user.toJson(),
      };
}
