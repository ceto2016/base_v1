import 'package:equatable/equatable.dart';

import '../../../../core/widgets/init_phone.dart';

class User extends Equatable {
  final int id;
  final String name;
  final String email;
  final String flag;
  final String code;
  final IntiPhoneNumber mobile;
  final String images;
  final bool isVerified;
  const User({
    required this.id,
    required this.name,
    required this.email,
    required this.mobile,
    required this.flag,
    required this.code,
    required this.images,
    required this.isVerified,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json['id'],
        name: json['name'],
        email: json['email'],
        mobile: IntiPhoneNumber.fromMap(json),
        // mobile: json['mobile'],
        images: json['images'],
        code: json['code'],
        flag: json['flag'],
        isVerified: json['is_verified'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': id,
        'name': name,
        'email': email,
        ...mobile.toMap(),
        'images': images,
        'is_verified': isVerified,
      };

  @override
  List<Object?> get props =>
      [id, name, email, mobile, images, isVerified, flag, code];
}
