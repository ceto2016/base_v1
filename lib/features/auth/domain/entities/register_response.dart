// To parse this JSON data, do
//
//     final registerResponse = registerResponseFromJson(jsonString);

import 'dart:convert';

import 'user.dart';

RegisterResponse registerResponseFromJson(String str) =>
    RegisterResponse.fromJson(json.decode(str));

String registerResponseToJson(RegisterResponse data) =>
    json.encode(data.toJson());

class RegisterResponse {
  final Data data;
  final int status;

  RegisterResponse({
    required this.data,
    required this.status,
  });

  factory RegisterResponse.fromJson(Map<String, dynamic> json) =>
      RegisterResponse(
        data: Data.fromJson(json['data']),
        status: json['status'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'data': data.toJson(),
        'status': status,
      };
}

class Data {
  final String verificationCode;
  final String accessToken;
  final User user;

  Data({
    required this.verificationCode,
    required this.accessToken,
    required this.user,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        verificationCode: json['verification_code'],
        accessToken: json['access_token'],
        user: User.fromJson(json['user']),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'verification_code': verificationCode,
        'access_token': accessToken,
        'user': user.toJson(),
      };
}
