// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import '../../../../../core/usecases/usecases.dart';
import '../../../../core/error/failures.dart';
import '../entities/login_response.dart';
import '../repositories/auth_repository.dart';

class LoginUseCase extends UseCase<LoginResponse, LoginParams> {
  final AuthRepository repository;
  LoginUseCase({required this.repository});
  @override
  Future<Either<Failure, LoginResponse>> call(LoginParams params) async {
    return await repository.login(params: params);
  }
}

class LoginParams {
  String phone;
  String password;
  String flag;
  String code;
  String? fcmToken;
  LoginParams({
    required this.phone,
    required this.password,
    required this.flag,
    required this.code,
    this.fcmToken,
  });
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'mobile': phone,
      'password': password,
      'code': code,
      'flag': flag,
      'fcm_token': fcmToken,
    };
  }

  factory LoginParams.fromMap(Map<String, dynamic> map) {
    return LoginParams(
      phone: map['mobile'],
      password: map['password'],
      flag: map['flag'],
      code: map['code'],
    );
  }
}
