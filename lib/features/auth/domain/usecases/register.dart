// ignore_for_file: constant_identifier_names

import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecases.dart';
import '../../../../injection_container.dart';
import '../entities/register_response.dart';
import '../repositories/auth_repository.dart';

class RegisterUsecase extends UseCase<RegisterResponse, RegisterParams> {
  final AuthRepository repository;

  RegisterUsecase({required this.repository});

  @override
  Future<Either<Failure, RegisterResponse>> call(RegisterParams params) async {
    return await repository.register(params: params);
  }
}

class RegisterParams {
  String name;
  String mobile;
  String flag;
  String code;
  String password;
  String passwordConfirmation;
  String? email;
  String terms;

  RegisterParams({
    required this.name,
    required this.password,
    required this.mobile,
    required this.flag,
    required this.code,
    required this.terms,
    required this.passwordConfirmation,
    this.email,
  });

  Map<String, dynamic> toMap() {
    // if (email != null || email != '') {
    //   // ignore: always_specify_types
    //   toMap().addAll({
    //     'email': email,
    //   });
    // }
    return <String, dynamic>{
      'name': name,
      'mobile': mobile,
      'password': password,
      'flag': flag,
      if (email != null || email != '') 'email': email,
      'code': code,
      'password_confirmation': passwordConfirmation,
      'terms_and_conditions': terms,
      'fcm_token': fcmToken,
    };
  }
}
