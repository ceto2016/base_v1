import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecases.dart';
import '../entities/create_new_pass.dart';
import '../repositories/auth_repository.dart';

class CreateNewPasswordUseCase
    extends UseCase<CreateNewPassResponse, CreateNewPasswordParams> {
  final AuthRepository repository;
  CreateNewPasswordUseCase({required this.repository});
  @override
  Future<Either<Failure, CreateNewPassResponse>> call(
      CreateNewPasswordParams params) async {
    return await repository.createNewPass(params: params);
  }
}

class CreateNewPasswordParams {
  final String pass;
  final String passConfirm;
  CreateNewPasswordParams({
    required this.pass,
    required this.passConfirm,
  });
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'password_confirmation': passConfirm,
      'password': pass,
    };
  }

  factory CreateNewPasswordParams.fromMap(Map<String, dynamic> map) {
    return CreateNewPasswordParams(
      passConfirm: map['password_confirmation'],
      pass: map['password'],
    );
  }
}
