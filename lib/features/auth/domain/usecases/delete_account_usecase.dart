// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecases.dart';
import '../repositories/auth_repository.dart';

class DeleteUserUseCase extends UseCase<String, DeleteParams> {
  final AuthRepository repository;
  DeleteUserUseCase({required this.repository});
  @override
  Future<Either<Failure, String>> call(DeleteParams params) async {
    return await repository.deleteUser(params: params);
  }
}

class DeleteParams {}
