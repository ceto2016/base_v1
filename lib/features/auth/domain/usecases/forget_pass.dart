import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecases.dart';
import '../../../../core/widgets/init_phone.dart';
import '../entities/forget_pass.dart';
import '../repositories/auth_repository.dart';

class ForgetPasswordUseCase
    extends UseCase<ForgetPassResponse, ForgetPasswordParams> {
  final AuthRepository repository;
  ForgetPasswordUseCase({required this.repository});
  @override
  Future<Either<Failure, ForgetPassResponse>> call(
      ForgetPasswordParams params) async {
    return await repository.forgetPassword(params: params);
  }
}

class ForgetPasswordParams {
  final IntiPhoneNumber phone;
  ForgetPasswordParams({
    required this.phone,
  });
}
