import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/util/api_basehelper.dart';
import '../../domain/entities/create_new_pass.dart';
import '../../domain/entities/forget_pass.dart';
import '../../domain/entities/login_response.dart';
import '../../domain/entities/register_response.dart';
import '../../domain/usecases/create_new_pass.dart';
import '../../domain/usecases/delete_account_usecase.dart';
import '../../domain/usecases/forget_pass.dart';
import '../../domain/usecases/login_usecase.dart';
import '../../domain/usecases/register.dart';

const String registerApi = '/auth/register';
const String loginAPI = '/auth/login';
const String logoutApi = '/auth/logout';
const String forgetPassApi = '/auth/forget-password';
const String createNewPassApi = '/auth/reset-password';
const String deleteUserApi = '/auth/delete-account';

abstract class AuthRemoteDataSource {
  Future<RegisterResponse> register({required RegisterParams params});
  Future<LoginResponse> login({required LoginParams params});
  Future<String> logout({
    required String token,
  });
  Future<ForgetPassResponse> forgetPassword(
      {required ForgetPasswordParams params});
  Future<CreateNewPassResponse> createNewPass(
      {required CreateNewPasswordParams params, required String token});
  Future<String> deleteUser(
      {required DeleteParams params, required String token});
}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  final ApiBaseHelper helper;
  AuthRemoteDataSourceImpl({
    required this.helper,
  });

  @override
  Future<RegisterResponse> register({
    required RegisterParams params,
  }) async {
    try {
      final Map<String, dynamic> response = await helper.post(
        url: registerApi,
        body: params.toMap(),
      ) as Map<String, dynamic>;
      if (response['status'] == 200) {
        final RegisterResponse res = RegisterResponse.fromJson(response);

        helper.addTokenInHeader(res.data.accessToken);
        return res;
      } else {
        throw ServerException(message: response['message']);
      }
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    }
  }

  @override
  Future<LoginResponse> login({required LoginParams params}) async {
    try {
      final response = await helper.post(url: loginAPI, body: params.toMap());
      if (response['status'] == 200) {
        final LoginResponse res = LoginResponse.fromJson(response);
        helper.addTokenInHeader(res.data.accessToken);
        return res;
      } else {
        throw ServerException(message: response['message']);
      }
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    }
  }

  @override
  Future<String> logout({
    required String token,
  }) async {
    try {
      final response =
          await helper.post(url: logoutApi, body: <String, dynamic>{});
      helper.removeTokenInHeader();
      return response['message'];
    } catch (e) {
      log(e.toString());

      String message = ('error_please_try_again'.tr());
      if (e is ServerException) {
        message = e.message;
      }
      throw ServerException(message: message);
    }
  }

  @override
  Future<ForgetPassResponse> forgetPassword(
      {required ForgetPasswordParams params}) async {
    try {
      final response =
          await helper.post(url: forgetPassApi, body: params.phone.toMap());
      final ForgetPassResponse data = ForgetPassResponse.fromJson(response);

      helper.addTokenInHeader(data.data.accessToken);
      return data;
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    }
  }

  @override
  Future<CreateNewPassResponse> createNewPass({
    required String token,
    required CreateNewPasswordParams params,
  }) async {
    try {
      final response =
          await helper.post(url: createNewPassApi, body: params.toMap());

      helper.removeTokenInHeader();
      final CreateNewPassResponse data =
          CreateNewPassResponse.fromJson(response);
      return data;
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    }
  }

  @override
  Future<String> deleteUser(
      {required DeleteParams params, required String token}) async {
    try {
      final dynamic response = await helper.delete(
        url: deleteUserApi,
      );
      helper.removeTokenInHeader();
      return response['message'];
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    }
  }
}
