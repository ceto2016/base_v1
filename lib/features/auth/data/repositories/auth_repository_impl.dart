// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/local/auth_local_datasource.dart';
import '../../../../injection_container.dart';
import '../../domain/entities/create_new_pass.dart';
import '../../domain/entities/forget_pass.dart';
import '../../domain/entities/login_response.dart';
import '../../domain/entities/register_response.dart';
import '../../domain/entities/user.dart';
import '../../domain/repositories/auth_repository.dart';
import '../../domain/usecases/create_new_pass.dart';
import '../../domain/usecases/delete_account_usecase.dart';
import '../../domain/usecases/forget_pass.dart';
import '../../domain/usecases/login_usecase.dart';
import '../../domain/usecases/register.dart';
import '../datasources/auth_remote_datasource.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource remote;
  final AuthLocalDataSource local;
  AuthRepositoryImpl({
    required this.remote,
    required this.local,
  });
  @override
  Future<Either<Failure, RegisterResponse>> register(
      {required RegisterParams params}) async {
    try {
      final RegisterResponse response = await remote.register(params: params);
      await local.cacheUserAccessToken(token: response.data.accessToken);

      await local.cacheUserLoginInfo(
          params: LoginParams(
              phone: params.mobile,
              password: params.password,
              flag: params.flag,
              code: params.code,
              fcmToken: fcmToken));

      return right(response);
    } on ServerException catch (e) {
      log(e.toString());
      return left(ServerFailure(
        message: e.message,
      ));
    } catch (e) {
      log(e.toString());
      return left(ServerFailure(message: tr('error_please_try_again')));
    }
  }

  @override
  Future<Either<Failure, LoginResponse>> login(
      {required LoginParams params}) async {
    try {
      local.cacheUserLoginInfo(params: params);
      params.fcmToken = fcmToken;
      final LoginResponse res = await remote.login(params: params);
      await local.cacheUserAccessToken(token: res.data.accessToken.toString());
      return right(res);
    } on ServerException catch (e) {
      return left(ServerFailure(message: e.message));
    } catch (e) {
      return left(ServerFailure(message: tr('error_please_try_again')));
    }
  }

  @override
  Future<Either<Failure, User>> autoLogin() async {
    try {
      final LoginParams loginInfo = local.getCacheUserLoginInfo();
      loginInfo.fcmToken = fcmToken;
      final LoginResponse userData = await remote.login(params: loginInfo);
      await local.cacheUserAccessToken(
          token: userData.data.accessToken.toString());
      return right(userData.data.user);
    } on ServerException catch (e) {
      log(e.toString());
      return left(ServerFailure(message: e.message));
    } on CacheException catch (e) {
      return left(CacheFailure(message: e.toString()));
    } catch (e) {
      log(e.toString());
      return left(ServerFailure(message: tr('error_please_try_again')));
    }
  }

  @override
  Future<Either<Failure, String>> logout() async {
    try {
      final String token = local.getCacheUserAccessToken();

      await remote.logout(
        token: token,
      );
      await local.clearData();
      return right('result');
    } on ServerException catch (e) {
      log(e.toString());
      await local.clearData();
      return right('result');
    } catch (e) {
      log(e.toString());
      return left(ServerFailure(message: tr('error_please_try_again')));
    }
  }

  @override
  Future<Either<Failure, ForgetPassResponse>> forgetPassword(
      {required ForgetPasswordParams params}) async {
    try {
      final ForgetPassResponse data =
          await remote.forgetPassword(params: params);
      await local.cacheUserAccessToken(token: data.data.accessToken.toString());
      return right(data);
    } on ServerException catch (e) {
      return left(ServerFailure(message: e.message));
    } catch (e) {
      log(e.toString());
      return left(ServerFailure(message: tr('error_please_try_again')));
    }
  }

  @override
  Future<Either<Failure, CreateNewPassResponse>> createNewPass(
      {required CreateNewPasswordParams params}) async {
    try {
      final String token = local.getCacheUserAccessToken();
      final CreateNewPassResponse res =
          await remote.createNewPass(params: params, token: token);

      return right(res);
    } on ServerException catch (e) {
      log(e.toString());
      return left(ServerFailure(message: e.message));
    } catch (e) {
      log(e.toString());
      return left(ServerFailure(message: tr('error_please_try_again')));
    }
  }

  @override
  Future<Either<Failure, String>> deleteUser(
      {required DeleteParams params}) async {
    try {
      final String token = local.getCacheUserAccessToken();

      await remote.deleteUser(token: token, params: params);

      return right('data');
    } on ServerException catch (e) {
      log(e.toString());
      return left(ServerFailure(message: e.message));
    } catch (e) {
      log(e.toString());
      return left(ServerFailure(message: tr('error_please_try_again')));
    }
  }
}
