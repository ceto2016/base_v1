import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../core/local/auth_local_datasource.dart';
import '../../injection_container.dart';
import 'data/datasources/auth_remote_datasource.dart';
import 'data/repositories/auth_repository_impl.dart';
import 'domain/repositories/auth_repository.dart';
import 'domain/usecases/auto_login.dart';
import 'domain/usecases/create_new_pass.dart';
import 'domain/usecases/delete_account_usecase.dart';
import 'domain/usecases/forget_pass.dart';
import 'domain/usecases/login_usecase.dart';
import 'domain/usecases/logout_usecase.dart';
import 'domain/usecases/register.dart';
import 'presentation/cubit/auto_login/auto_login_cubit.dart';
import 'presentation/cubit/delete_user/delete_user_cubit.dart';
import 'presentation/cubit/forget_pass/forget_pass_cubit.dart';
import 'presentation/cubit/login/login_cubit_cubit.dart';
import 'presentation/cubit/logout/logout_cubit.dart';
import 'presentation/cubit/new_password/new_password_cubit.dart';
import 'presentation/cubit/register/register_cubit.dart';

void initAuthInjection() async {
  //* Blocs
  sl.registerLazySingleton(() => RegisterCubit(
        usecase: sl(),
      ));
  sl.registerLazySingleton(() => LoginCubit(sl()));
  sl.registerLazySingleton(() => AutoLoginCubit(autoLogin: sl()));
  sl.registerLazySingleton(() => LogoutCubit(logout: sl()));
  sl.registerFactory(() => ForgetPassCubit(sl()));
  sl.registerFactory(
      () => CreateNewPasswordCubit(createNewPasswordUseCase: sl()));
  sl.registerLazySingleton(() => DeleteUserCubit(deleteUser: sl()));

  //* Use cases
  sl.registerLazySingleton(() => RegisterUsecase(repository: sl()));
  sl.registerLazySingleton(() => LoginUseCase(repository: sl()));
  sl.registerLazySingleton(() => AutoLoginUsecase(repository: sl()));
  sl.registerLazySingleton(() => LogoutUseCase(repository: sl()));
  sl.registerLazySingleton(() => ForgetPasswordUseCase(repository: sl()));
  sl.registerLazySingleton(() => CreateNewPasswordUseCase(repository: sl()));
  sl.registerLazySingleton(() => DeleteUserUseCase(repository: sl()));

  //* Repository
  sl.registerLazySingleton<AuthRepository>(
      () => AuthRepositoryImpl(remote: sl(), local: sl()));

  //* Data sources
  sl.registerLazySingleton<AuthRemoteDataSource>(() => AuthRemoteDataSourceImpl(
        helper: sl(),
      ));
  sl.registerLazySingleton<AuthLocalDataSource>(
      () => AuthLocalDataSourceImpl(sharedPreference: sl()));
}

List<BlocProvider<Cubit<Object>>> authBlocs(BuildContext context) =>
    <BlocProvider<Cubit<Object>>>[
      BlocProvider<LoginCubit>(
          create: (BuildContext context) => sl<LoginCubit>()),
      BlocProvider<RegisterCubit>(
        create: (BuildContext context) => sl<RegisterCubit>(),
      ),
      BlocProvider<AutoLoginCubit>(
          create: (BuildContext context) => sl<AutoLoginCubit>()),
      BlocProvider<LogoutCubit>(
          create: (BuildContext context) => sl<LogoutCubit>()),
      // BlocProvider<CreateNewPasswordCubit>(
      //     create: (BuildContext context) => sl<CreateNewPasswordCubit>()),
      BlocProvider<DeleteUserCubit>(
          create: (BuildContext context) => sl<DeleteUserCubit>()),
    ];
