import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../injection_container.dart';
import 'data/datasources/more_datasource.dart';
import 'data/repositories/more_repo_impl.dart';
import 'domain/repo/more_repo.dart';
import 'domain/usecase/change_password.dart';
import 'domain/usecase/contact_us.dart';
import 'domain/usecase/get_contact_info.dart';
import 'domain/usecase/get_profile.dart';
import 'domain/usecase/get_welcome_video.dart';
import 'domain/usecase/terms.dart';
import 'domain/usecase/update_profile.dart';

void initMoreInjection() async {
  //* Blocs

  // sl.registerLazySingleton(() => GetWelcomeVideoCubit(sl()));
  // sl.registerFactory(() => UpdateProfileCubit(sl()));
  // sl.registerFactory(() => ContactUsCubit(sl()));
  // sl.registerLazySingleton(() => GetStaticPagesCubit(sl()));
  // sl.registerLazySingleton(() => GetContactInfoCubit(getContactInfo: sl()));
  // sl.registerLazySingleton(() => GetProfileCubit(getProfile: sl()));
  // sl.registerLazySingleton(() => ChangePasswordCubit(sl()));

  //* Use cases
  sl.registerLazySingleton(() => GetWelcomeVideo(repository: sl()));
  sl.registerLazySingleton(() => UpdateProfile(repository: sl()));
  sl.registerLazySingleton(() => ContactUs(repository: sl()));
  sl.registerLazySingleton(() => GetContactInfo(repository: sl()));
  sl.registerLazySingleton(() => StaticPagesUsecase(repository: sl()));
  sl.registerLazySingleton(() => GetProfile(repository: sl()));
  sl.registerLazySingleton(() => ChangePasswordUseCase(repository: sl()));

  //* Repository
  sl.registerLazySingleton<MoreRepository>(
      () => MoreRepositoryImpl(remote: sl(), authLocal: sl()));

  //* Data sources
  sl.registerLazySingleton<MoreRemoteDataSource>(
      () => MoreRemoteDataSourceImpl(helper: sl()));
}

List<BlocProvider<Cubit<Object>>> moreBlocs(BuildContext _) =>
    <BlocProvider<Cubit<Object>>>[
      // BlocProvider<GetWelcomeVideoCubit>(
      //     create: (BuildContext context) => sl<GetWelcomeVideoCubit>()),
    ];
