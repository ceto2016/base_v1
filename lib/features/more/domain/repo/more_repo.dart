import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../auth/domain/entities/user.dart';
import '../entities/contact_info_response.dart';
import '../entities/profile_settings.dart';
import '../entities/static_pages_entity.dart';
import '../usecase/change_password.dart';
import '../usecase/contact_us.dart';
import '../usecase/terms.dart';
import '../usecase/update_profile.dart';

abstract class MoreRepository {
  Future<Either<Failure, StaticPagesResponse>> getStaticPages(
      {required SettingeParams params});
  Future<Either<Failure, ProfileResponse>> getProfile();
  Future<Either<Failure, String>> getWelcomeVideo();
  Future<Either<Failure, ContactInfoResponse>> getContactInfo();
  Future<Either<Failure, Unit>> contactUs({required ContactUsParams params});
  Future<Either<Failure, User>> updateProfile(
      {required UpdateProfileParams params});
  Future<Either<Failure, Unit>> changePassword(
      {required ChangePasswordParams params});
}
