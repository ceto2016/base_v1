// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import '../../../../../core/usecases/usecases.dart';
import '../../../../core/error/failures.dart';
import '../entities/contact_info_response.dart';
import '../repo/more_repo.dart';

class GetContactInfo extends UseCase<ContactInfoResponse, NoParams> {
  final MoreRepository repository;
  GetContactInfo({required this.repository});
  @override
  Future<Either<Failure, ContactInfoResponse>> call(NoParams params) async {
    return await repository.getContactInfo();
  }
}
