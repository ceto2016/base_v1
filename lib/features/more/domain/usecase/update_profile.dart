// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import '../../../../../core/usecases/usecases.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/widgets/init_phone.dart';
import '../../../auth/domain/entities/user.dart';
import '../repo/more_repo.dart';

class UpdateProfile extends UseCase<User, UpdateProfileParams> {
  final MoreRepository repository;
  UpdateProfile({required this.repository});
  @override
  Future<Either<Failure, User>> call(UpdateProfileParams params) async {
    return await repository.updateProfile(params: params);
  }
}

class UpdateProfileParams {
  final String email;
  final String name;
  final IntiPhoneNumber phone;

  UpdateProfileParams({
    required this.email,
    required this.name,
    required this.phone,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'email': email,
      'name': name,
      ...phone.toMap(),
    }..removeWhere((key, value) => value == null || value == '');
  }
}
