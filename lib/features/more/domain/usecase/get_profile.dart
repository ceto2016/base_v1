// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import '../../../../../core/usecases/usecases.dart';
import '../../../../core/error/failures.dart';
import '../entities/profile_settings.dart';
import '../repo/more_repo.dart';

class GetProfile extends UseCase<ProfileResponse, NoParams> {
  final MoreRepository repository;
  GetProfile({required this.repository});
  @override
  Future<Either<Failure, ProfileResponse>> call(NoParams params) async {
    return await repository.getProfile();
  }
}
