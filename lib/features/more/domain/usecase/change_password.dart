import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecases.dart';
import '../repo/more_repo.dart';

class ChangePasswordUseCase extends UseCase<Unit, ChangePasswordParams> {
  final MoreRepository repository;
  ChangePasswordUseCase({required this.repository});
  @override
  Future<Either<Failure, Unit>> call(ChangePasswordParams params) async {
    return await repository.changePassword(params: params);
  }
}

class ChangePasswordParams {
  final String oldPassword;
  final String newPassword;
  final String confirmPassword;

  ChangePasswordParams({
    required this.oldPassword,
    required this.newPassword,
    required this.confirmPassword,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'old_password': oldPassword,
      'password': newPassword,
      'password_confirmation': confirmPassword,
    };
  }
}
