// ignore_for_file: constant_identifier_names

import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecases.dart';
import '../entities/static_pages_entity.dart';
import '../repo/more_repo.dart';

class StaticPagesUsecase extends UseCase<StaticPagesResponse, SettingeParams> {
  final MoreRepository repository;

  StaticPagesUsecase({required this.repository});

  @override
  Future<Either<Failure, StaticPagesResponse>> call(
      SettingeParams params) async {
    return await repository.getStaticPages(params: params);
  }
}

class SettingeParams {
  final SettingsFilter type;

  SettingeParams({required this.type});
}

enum SettingsFilter {
  terms_and_conditions,
  about_us,
  privacy_policy,
  instructions,
}
