// ignore_for_file: public_member_api_docs, sort_constructors_first, constant_identifier_names
import 'package:dartz/dartz.dart';

import '../../../../../core/usecases/usecases.dart';
import '../../../../core/error/failures.dart';
import '../repo/more_repo.dart';

class ContactUs extends UseCase<Unit, ContactUsParams> {
  final MoreRepository repository;
  ContactUs({required this.repository});
  @override
  Future<Either<Failure, Unit>> call(ContactUsParams params) async {
    return await repository.contactUs(params: params);
  }
}

class ContactUsParams {
  final String name;
  final String email;
  final String phone;
  final String message;
  final ContactUsType messageType;

  ContactUsParams(
      {required this.name,
      required this.email,
      required this.phone,
      required this.message,
      required this.messageType});

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'email': email,
      'phone': phone,
      'message': message,
      'message_type': messageType.name,
    };
  }
}

enum ContactUsType {
  Request('request'),
  Suggestion('suggestion'),
  Inquiry('inquiry'),
  Complaint('complaint'),
  Other('other');

  final String text;
  const ContactUsType(this.text);
}
