// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dartz/dartz.dart';

import '../../../../../core/usecases/usecases.dart';
import '../../../../core/error/failures.dart';
import '../repo/more_repo.dart';

class GetWelcomeVideo extends UseCase<String, NoParams> {
  final MoreRepository repository;
  GetWelcomeVideo({required this.repository});
  @override
  Future<Either<Failure, String>> call(NoParams params) async {
    return await repository.getWelcomeVideo();
  }
}
