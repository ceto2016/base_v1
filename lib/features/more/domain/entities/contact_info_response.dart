// ignore_for_file: public_member_api_docs, sort_constructors_first
// To parse this JSON data, do
//
//     final contactInfoResponse = contactInfoResponseFromJson(jsonString);

import 'dart:convert';

ContactInfoResponse contactInfoResponseFromJson(String str) =>
    ContactInfoResponse.fromJson(json.decode(str));

String contactInfoResponseToJson(ContactInfoResponse data) =>
    json.encode(data.toJson());

class ContactInfoResponse {
  final int status;
  final String message;
  final ContactData data;

  ContactInfoResponse({
    required this.status,
    required this.message,
    required this.data,
  });

  factory ContactInfoResponse.fromJson(Map<String, dynamic> json) =>
      ContactInfoResponse(
        status: json['status'],
        message: json['message'],
        data: ContactData.fromJson(json['data']),
      );

  Map<String, dynamic> toJson() => {
        'status': status,
        'message': message,
        'data': data.toJson(),
      };
}

class ContactData {
  final List<String> phones;
  final List<String> whatsup;
  final String faceBook;
  final String twitter;
  final String snapchat;
  final String tiktok;
  final String instagram;

  final String video;

  ContactData({
    required this.phones,
    required this.whatsup,
    required this.faceBook,
    required this.twitter,
    required this.snapchat,
    required this.tiktok,
    required this.instagram,
    required this.video,
  });

  factory ContactData.fromJson(Map<String, dynamic> json) => ContactData(
        phones: json['phones'] == null || json['phones'] is Map
            ? []
            : List<String>.from(json['phones'].map((x) => x)),
        faceBook: json['face_book'],
        twitter: json['twitter'] ?? '',
        snapchat: json['snapchat'] ?? '',
        instagram: json['instagram'] ?? '',
        tiktok: json['tiktok'] ?? '',
        video: json['video'] ?? '',
        whatsup: json['whatsup'] == null || json['whatsup'] is Map
            ? []
            : List<String>.from(json['whatsup'].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        'phones': List<dynamic>.from(phones.map((x) => x)),
        'face_book': faceBook,
        'twitter': twitter,
        'whatsup': List<dynamic>.from(whatsup.map((x) => x)),
      };
}
