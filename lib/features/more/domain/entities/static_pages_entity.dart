// To parse this JSON data, do
//
//     final staticPagesResponse = staticPagesResponseFromJson(jsonString);

import 'dart:convert';

StaticPagesResponse staticPagesResponseFromJson(String str) =>
    StaticPagesResponse.fromJson(json.decode(str));

String staticPagesResponseToJson(StaticPagesResponse data) =>
    json.encode(data.toJson());

class StaticPagesResponse {
  final StaticPagesData data;
  final int status;

  StaticPagesResponse({
    required this.data,
    required this.status,
  });

  factory StaticPagesResponse.fromJson(Map<String, dynamic> json) =>
      StaticPagesResponse(
        data: StaticPagesData.fromJson(
            json['data'] is List ? json['data'].first : json['data']),
        status: json['status'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'data': data.toJson(),
        'status': status,
      };
}

class StaticPagesData {
  final String key;
  final String name;
  final String value;

  StaticPagesData({
    required this.key,
    required this.name,
    required this.value,
  });

  factory StaticPagesData.fromJson(Map<String, dynamic> json) =>
      StaticPagesData(
        key: json['key'],
        name: json['name'],
        value: json['value'] ?? '',
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'key': key,
        'name': name,
        'value': value,
      };
}
