import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/local/auth_local_datasource.dart';
import '../../../../core/util/api_basehelper.dart';
import '../../../../injection_container.dart';
import '../../domain/entities/contact_info_response.dart';
import '../../domain/entities/profile_settings.dart';
import '../../domain/entities/static_pages_entity.dart';
import '../../domain/usecase/change_password.dart';
import '../../domain/usecase/contact_us.dart';
import '../../domain/usecase/terms.dart';
import '../../domain/usecase/update_profile.dart';

const String getStaticPagesApi = '/get-settings';
const String getContactInfoApi = '/contact-info';
const String contactUsApi = '/contact-us/create';
const String updateProfileApi = '/auth/save-profile';
const String getWelcomeVideoApi = '/get-video';

const String getProfileAPI = '/auth/profile';
const String changePasswordAPI = '/auth/chang-password';

abstract class MoreRemoteDataSource {
  Future<StaticPagesResponse> staticPages({required SettingeParams params});
  Future<ContactInfoResponse> getContactInfo();

  Future<ProfileResponse> getProfile();
  Future<void> contactUs({required ContactUsParams params});
  Future<void> updateProfile({required UpdateProfileParams params});
  Future<String> getWelcomeVideo();
  Future<void> changePassword({required ChangePasswordParams params});
}

class MoreRemoteDataSourceImpl implements MoreRemoteDataSource {
  final ApiBaseHelper helper;

  MoreRemoteDataSourceImpl({required this.helper});

  @override
  Future<StaticPagesResponse> staticPages(
      {required SettingeParams params}) async {
    try {
      final dynamic response =
          await helper.get(url: '$getStaticPagesApi?key=${params.type.name}');
      final StaticPagesResponse terms = StaticPagesResponse.fromJson(response);

      return terms;
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    } catch (e) {
      log(e.toString());
      throw ServerException(message: tr('error_no_internet'));
    }
  }

  @override
  Future<ProfileResponse> getProfile() async {
    try {
      final response = await helper.get(
        url: getProfileAPI,
      );
      if (response['status'] == 200) {
        return ProfileResponse.fromJson(response);
      } else {
        throw ServerException(message: response['message']);
      }
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    } catch (e) {
      log(e.toString());
      throw ServerException(message: tr('error_no_internet'));
    }
  }

  @override
  Future<ContactInfoResponse> getContactInfo() async {
    try {
      final response = await helper.get(url: getContactInfoApi);
      return ContactInfoResponse.fromJson(response);
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    } catch (e) {
      log(e.toString());
      throw ServerException(message: tr('error_no_internet'));
    }
  }

  @override
  Future<void> contactUs({required ContactUsParams params}) async {
    try {
      await helper.post(
        url: contactUsApi,
        body: params.toMap(),
      );
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    } catch (e) {
      log(e.toString());
      throw ServerException(message: tr('error_no_internet'));
    }
  }

  @override
  Future<void> updateProfile({required UpdateProfileParams params}) async {
    try {
      await helper.post(
        url: updateProfileApi,
        body: params.toMap(),
      );
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    } catch (e) {
      log(e.toString());
      throw ServerException(message: tr('error_no_internet'));
    }
  }

  @override
  Future<String> getWelcomeVideo() async {
    try {
      final response = await helper.get(url: getWelcomeVideoApi);
      return response['data']['video'];
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    } catch (e) {
      log(e.toString());
      throw ServerException(message: tr('error_no_internet'));
    }
  }

  @override
  Future<void> changePassword({required ChangePasswordParams params}) async {
    try {
      final res =
          await helper.post(url: changePasswordAPI, body: params.toMap());
      helper.addTokenInHeader(res['access_token']);
      sl<AuthLocalDataSource>()
          .cacheUserAccessToken(token: res['access_token']);
    } on ServerException catch (e) {
      throw ServerException(message: e.message);
    } catch (e) {
      log(e.toString());
      throw ServerException(message: tr('error_no_internet'));
    }
  }
}
