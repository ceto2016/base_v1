import 'dart:developer';

import 'package:dartz/dartz.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/local/auth_local_datasource.dart';
import '../../../auth/domain/entities/user.dart';
import '../../domain/entities/contact_info_response.dart';
import '../../domain/entities/profile_settings.dart';
import '../../domain/entities/static_pages_entity.dart';
import '../../domain/repo/more_repo.dart';
import '../../domain/usecase/change_password.dart' show ChangePasswordParams;
import '../../domain/usecase/contact_us.dart';
import '../../domain/usecase/terms.dart';
import '../../domain/usecase/update_profile.dart';
import '../datasources/more_datasource.dart';

class MoreRepositoryImpl implements MoreRepository {
  final MoreRemoteDataSource remote;
  final AuthLocalDataSource authLocal;

  MoreRepositoryImpl({required this.remote, required this.authLocal});

  @override
  Future<Either<Failure, StaticPagesResponse>> getStaticPages(
      {required SettingeParams params}) async {
    try {
      final StaticPagesResponse response =
          await remote.staticPages(params: params);
      return right(response);
    } on ServerException catch (e) {
      log(e.toString());
      return left(ServerFailure(message: e.message));
    }
  }

  @override
  Future<Either<Failure, ContactInfoResponse>> getContactInfo() async {
    try {
      final ContactInfoResponse response = await remote.getContactInfo();
      return right(response);
    } on ServerException catch (e) {
      log(e.toString());
      return left(ServerFailure(message: e.message));
    }
  }

  @override
  Future<Either<Failure, ProfileResponse>> getProfile() async {
    try {
      final ProfileResponse res = await remote.getProfile();
      return right(res);
    } on ServerException catch (e) {
      return left(ServerFailure(message: e.message));
    } on CacheException catch (e) {
      return left(CacheFailure(message: e.toString()));
    }
  }

  @override
  Future<Either<Failure, Unit>> contactUs(
      {required ContactUsParams params}) async {
    try {
      await remote.contactUs(params: params);
      return right(unit);
    } on ServerException catch (e) {
      return left(ServerFailure(message: e.message));
    } on CacheException catch (e) {
      return left(CacheFailure(message: e.toString()));
    }
  }

  @override
  Future<Either<Failure, User>> updateProfile(
      {required UpdateProfileParams params}) async {
    try {
      await remote.updateProfile(params: params);
      final user = await remote.getProfile();
      return right(user.data.user);
    } on ServerException catch (e) {
      return left(ServerFailure(message: e.message));
    } on CacheException catch (e) {
      return left(CacheFailure(message: e.toString()));
    }
  }

  @override
  @override
  Future<Either<Failure, String>> getWelcomeVideo() async {
    try {
      final video = await remote.getWelcomeVideo();

      return right(video);
    } on ServerException catch (e) {
      return left(ServerFailure(message: e.message));
    } on CacheException catch (e) {
      return left(CacheFailure(message: e.toString()));
    }
  }

  @override
  Future<Either<Failure, Unit>> changePassword(
      {required ChangePasswordParams params}) async {
    try {
      await remote.changePassword(params: params);

      return right(unit);
    } on ServerException catch (e) {
      return left(ServerFailure(message: e.message));
    } on CacheException catch (e) {
      return left(CacheFailure(message: e.toString()));
    }
  }
}
