import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../injection_container.dart';
import 'data/datasources/books_remote_datasource.dart';
import 'data/repositories/books_repo_impl.dart';
import 'domain/repositories/books_repo.dart';
import 'domain/usecases/get_books_usecase.dart';
import 'presentation/controller/get_books/get_books_cubit.dart';

Future<void> initBooksInjection() async {
  /// Cubits
  sl.registerLazySingleton(() => GetBooksCubit(sl()));

  /// UseCases
  sl.registerLazySingleton(() => GetBooksUseCase(repository: sl()));

  /// Repository
  sl.registerLazySingleton<BooksRepository>(
      () => BooksRepositoryImpl(remote: sl()));

  /// DataSource
  sl.registerLazySingleton<BooksRemoteDataSource>(
      () => BooksRemoteDataSourceImpl());
}

/// BlocProviders
List<BlocProvider<StateStreamableSource<Object?>>> booksBlocs(
        BuildContext context) =>
    <BlocProvider<StateStreamableSource<Object?>>>[
      BlocProvider<GetBooksCubit>(
        create: (BuildContext context) => sl<GetBooksCubit>(),
      ),
    ];
