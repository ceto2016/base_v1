part of 'get_books_cubit.dart';


abstract class GetBooksState extends Equatable {

  const GetBooksState();

  @override
  List<Object?> get props => <Object?>[];
}

class GetBooksInitialState extends GetBooksState {
  const GetBooksInitialState();
}

class GetBooksLoadingState extends GetBooksState {
  const GetBooksLoadingState();
}

class GetBooksSuccessState extends GetBooksState {
  final List<Book> value;

  const GetBooksSuccessState({required this.value});

  @override
  List<Object?> get props => <Object?>[value];
}

class GetBooksErrorState extends GetBooksState {
  final String message;

  const GetBooksErrorState({required this.message});

  @override
  List<Object?> get props => <Object?>[message];
}

