import 'package:dartz/dartz.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/usecases/usecases.dart';
import '../../../domain/entities/get_books_response.dart';
import '../../../domain/usecases/get_books_usecase.dart';

part 'get_books_states.dart';


class GetBooksCubit extends Cubit<GetBooksState> {
  final GetBooksUseCase getBooksUseCase;

  GetBooksCubit(this.getBooksUseCase) : super(const GetBooksInitialState());

  List<Book> data = <Book>[];

  Future<void> fGetBooks() async {
    emit(const GetBooksLoadingState());
    final Either<Failure, GetBooksResponse> eitherResult = await getBooksUseCase.call(NoParams());
    eitherResult.fold((Failure fail) {
      String message = 'somethingWentWrong'.tr();
      if (fail is ServerFailure) {
        message = fail.message;
      }
      emit(GetBooksErrorState(message: message));
    }, (GetBooksResponse response) {
      data.clear();
      data.addAll(response.data);
      emit(GetBooksSuccessState(value: response.data));
    });
  }
}

