import '../../domain/entities/get_books_response.dart';

class GetBooksModel extends GetBooksResponse {
  const GetBooksModel({
    required super.links,
    required super.meta,
    required super.status,
    required super.data,
  });

  factory GetBooksModel.fromJson(Map<String, dynamic> json) => GetBooksModel(
        links: json['links'],
        meta: json['meta'],
        status: json['status'],
        data: (json['data'] as List<dynamic>)
            .map((dynamic e) => BookModel.fromJson(e))
            .toList(),
      );
}

class BookModel extends Book {
  const BookModel({
    required super.authorName,
    required super.createdAt,
    required super.id,
    required super.isActive,
    required super.name,
    required super.src,
    required super.status,
  });

  factory BookModel.fromJson(Map<String, dynamic> json) => BookModel(
        createdAt:
            DateTime.tryParse(json['created_at'] ?? '') ?? DateTime.now(),
        // json['created_at'] ?? '',
        id: json['id'] != null
            ? num.tryParse(json['id'].toString())?.toInt() ?? 0
            : 0,
        isActive: json['is_active'] != null
            ? num.tryParse(json['is_active'].toString())?.toInt() ?? 0
            : 0,
        name: json['name'] ?? '',
        src: json['src'] ?? '',
        status: json['status'] ?? '',
        authorName: json['author_name'] ?? '',
      );

  static Map<String, dynamic> toJson(Book? value) => <String, dynamic>{
        'created_at': value?.createdAt?.toIso8601String(),
        'id': value?.id,
        'is_active': value?.isActive,
        'name': value?.name,
        'src': value?.src,
        'status': value?.status,
        'author_name': value?.authorName,
      };
}
