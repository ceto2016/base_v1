import 'dart:developer';

import 'package:dartz/dartz.dart';

import '../../data/datasources/books_remote_datasource.dart';
import '../../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../domain/repositories/books_repo.dart';
import '../../domain/entities/get_books_response.dart';
import '../../../../core/usecases/usecases.dart';


class BooksRepositoryImpl implements BooksRepository {
  final BooksRemoteDataSource remote;

  BooksRepositoryImpl({
    required this.remote,
  });

  /// Impl
  @override
  Future<Either<Failure, GetBooksResponse>> getBooks({required NoParams params}) async {
    try {
      final GetBooksResponse response = await remote.getBooks(
        params: params,
      );
      return Right<Failure, GetBooksResponse>(response);
    } on ServerException catch (e) {
      log(e.toString());
      return Left<Failure, GetBooksResponse>(ServerFailure(message: e.message));
    }
  }


}

