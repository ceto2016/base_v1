import 'dart:developer';

import 'package:easy_localization/easy_localization.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/usecases/usecases.dart';
import '../../../../injection_container.dart';
import '../models/get_books_model.dart';

abstract class BooksRemoteDataSource {
  Future<GetBooksModel> getBooks({
    required NoParams params,
  });
}

class BooksRemoteDataSourceImpl implements BooksRemoteDataSource {
  final String getBooksEndpoint = '/get-books';
  @override
  Future<GetBooksModel> getBooks({
    required NoParams params,
  }) async {
    try {
      final dynamic response = await helper.get(
        url: getBooksEndpoint,
      );

      if (response['status'] == 200) {
        log('$getBooksEndpoint ${response.toString()}');
        return GetBooksModel.fromJson(response);
      } else {
        throw ServerException(message: response['message']);
      }
    } catch (e) {
      log(e.toString());
      String message = tr('error_please_try_again');
      if (e is ServerException) {
        message = e.message;
      }
      throw ServerException(message: message);
    }
  }
}
