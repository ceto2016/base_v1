import 'package:equatable/equatable.dart';

class GetBooksResponse extends Equatable {
  final Map<String, dynamic> links;
  final Map<String, dynamic> meta;
  final int status;
  final List<Book> data;

  const GetBooksResponse({
    required this.links,
    required this.meta,
    required this.status,
    required this.data,
  });

  @override
  List<Object?> get props => <Object?>[
        links,
        meta,
        status,
        data,
      ];
}

class Book extends Equatable {
  final DateTime? createdAt;
  final int id;
  final int isActive;
  final String name;
  final String authorName;
  final String src;
  final dynamic status;

  const Book({
    required this.authorName,
    this.createdAt,
    required this.id,
    required this.isActive,
    required this.name,
    required this.src,
    required this.status,
  });

  Book copyWith({
    DateTime? createdAt,
    int? id,
    int? isActive,
    String? name,
    String? src,
    String? authorName,
    dynamic status,
  }) {
    return Book(
      createdAt: createdAt ?? this.createdAt,
      id: id ?? this.id,
      isActive: isActive ?? this.isActive,
      name: name ?? this.name,
      src: src ?? this.src,
      status: status ?? this.status,
      authorName: authorName ?? this.authorName,
    );
  }

  @override
  List<Object?> get props => <Object?>[
        createdAt,
        id,
        isActive,
        name,
        src,
        status,
        authorName,
      ];
}
