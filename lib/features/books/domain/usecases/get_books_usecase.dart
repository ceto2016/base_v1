import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecases.dart';
import '../entities/get_books_response.dart';
import '../repositories/books_repo.dart';


class GetBooksUseCase extends UseCase<GetBooksResponse, NoParams> {
  final BooksRepository repository;

  GetBooksUseCase({required this.repository});

  @override
  Future<Either<Failure, GetBooksResponse>> call(NoParams params) async {
    return await repository.getBooks(params: params);
  }
}



