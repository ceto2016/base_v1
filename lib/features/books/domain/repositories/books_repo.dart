import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../domain/entities/get_books_response.dart';
import '../../../../core/usecases/usecases.dart';


abstract class BooksRepository {
  Future<Either<Failure, GetBooksResponse>> getBooks({
    required NoParams params,
  });


}

