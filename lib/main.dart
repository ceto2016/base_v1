// import 'package:alarm/alarm.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'core/services/bloc_observer/bloc_observer.dart';
import 'core/services/crashlytics/firebase_crashlytics.dart';
import 'core/services/services/notifications/firebase_messaging.dart';
import 'core/services/services/notifications/local_notifications.dart';
import 'injection_container.dart' as di;
import 'main_widget.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseCrashlyticsService.run();
  await EasyLocalization.ensureInitialized();
  await di.init();
  Bloc.observer = BlocObserverService();

  SystemChrome.setPreferredOrientations(
      <DeviceOrientation>[DeviceOrientation.portraitUp]);

  ///FCM
  final LocalNotificationService localNotificationService =
      di.sl<LocalNotificationService>();
  await localNotificationService.initialize();
  AppFirebaseMessaging.setForegroundNotificationPresentationOptions();
  AppFirebaseMessaging.onMessage(localNotificationService);
  AppFirebaseMessaging.onMessageOpenedApp(localNotificationService);
  await AppFirebaseMessaging.getToken();
  AppFirebaseMessaging.onTokenRefresh();
  AppFirebaseMessaging.subscribeToTopic('all');

  runApp(
    EasyLocalization(
      supportedLocales: const <Locale>[
        Locale('ar'),
        Locale('en'),
        Locale('ur'),
      ],
      path: 'assets/lang',
      fallbackLocale: const Locale('ar'),
      startLocale: const Locale('ar'),
      assetLoader: const RootBundleAssetLoader(),
      child: const MyApp(),
    ),
  );
}
